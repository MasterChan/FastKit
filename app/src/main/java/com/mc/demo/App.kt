package com.mc.demo

import android.app.Application
import com.blankj.utilcode.util.Utils

/**
 * App
 * @author: MasterChan
 * @date: 2023-02-21 18:01:11
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        com.mc.fastkit.FastKit.Companion.init(this).configLog { setSaveLog(true) }
        Utils.init(this)
    }
}