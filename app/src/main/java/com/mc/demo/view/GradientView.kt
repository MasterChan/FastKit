package com.mc.demo.view

import android.content.Context
import android.graphics.drawable.GradientDrawable.Orientation
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.SeekBar
import androidx.lifecycle.MutableLiveData
import com.mc.demo.databinding.ViewGradientBinding
import com.mc.fastkit.view.shape.GradientType
import com.mc.fastkit.callback.SimpleSeekBarChangeListener

/**
 * GradientView
 * @author: MasterChan
 * @date: 2023-11-06 16:57
 */
class GradientView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val binding = ViewGradientBinding.inflate(LayoutInflater.from(context), this, true)
    val radiusData = MutableLiveData<Int>()
    val strokeWidthData = MutableLiveData<Int>()
    val dashWidthData = MutableLiveData<Int>()
    val dashGapWidthData = MutableLiveData<Int>()
    val gradientTypeData = MutableLiveData<Int>()
    val gradientOrientationData = MutableLiveData<Orientation>()
    val gradientRadiusData = MutableLiveData<Int>()
    val gradientCenterXData = MutableLiveData<Float>()
    val gradientCenterYData = MutableLiveData<Float>()

    init {
        binding.sbRadius.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                radiusData.postValue(progress)
            }
        })
        binding.sbStrokeSize.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                strokeWidthData.postValue(progress)
            }
        })
        binding.sbDashSize.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                dashWidthData.postValue(progress)
            }
        })
        binding.sbDashGap.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                dashGapWidthData.postValue(progress)
            }
        })
        binding.rgGradientType.setOnCheckedChangeListener { _, checkedId ->
            gradientTypeData.postValue(
                when (checkedId) {
                    binding.rbTypeNone.id -> GradientType.NONE
                    binding.rbTypeLinear.id -> GradientType.LINEAR
                    binding.rbTypeRadial.id -> GradientType.RADIAL
                    binding.rbTypeSweep.id -> GradientType.SWEEP
                    else -> GradientType.NONE
                }
            )
        }
        binding.rgGradientOrientation.setOnCheckedChangeListener { _, checkedId ->
            gradientOrientationData.postValue(
                when (checkedId) {
                    binding.rbTb.id -> Orientation.TOP_BOTTOM
                    binding.rbTrBl.id -> Orientation.TR_BL
                    binding.rbRl.id -> Orientation.RIGHT_LEFT
                    binding.rbBrTl.id -> Orientation.BR_TL
                    binding.rbBt.id -> Orientation.BOTTOM_TOP
                    binding.rbBlTr.id -> Orientation.BL_TR
                    binding.rbLr.id -> Orientation.LEFT_RIGHT
                    binding.rbTlBr.id -> Orientation.TL_BR
                    else -> Orientation.TOP_BOTTOM
                }
            )
        }
        binding.sbGradientRadius.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                gradientRadiusData.postValue(progress)
            }
        })
        binding.sbCenterX.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                gradientCenterXData.postValue(progress / 100f)
            }
        })
        binding.sbCenterY.setOnSeekBarChangeListener(object : SimpleSeekBarChangeListener() {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                gradientCenterYData.postValue(progress / 100f)
            }
        })
    }

    fun setRadius(radius: Int) {
        binding.sbRadius.progress = radius
    }

    fun setStrokeWidth(width: Int) {
        binding.sbStrokeSize.progress = width
    }

    fun setDashWidth(width: Int) {
        binding.sbDashSize.progress = width
    }

    fun setDashGapWidth(width: Int) {
        binding.sbDashGap.progress = width
    }

    fun setGradientType(type: Int) {
        binding.rgGradientType.check(
            when (type) {
                GradientType.NONE -> binding.rbTypeNone.id
                GradientType.LINEAR -> binding.rbTypeLinear.id
                GradientType.RADIAL -> binding.rbTypeRadial.id
                GradientType.SWEEP -> binding.rbTypeSweep.id
                else -> 0
            }
        )
    }

    fun setGradientOrientation(orientation: Orientation) {
        binding.rgGradientOrientation.check(
            when (orientation) {
                Orientation.TOP_BOTTOM -> binding.rbTb.id
                Orientation.TR_BL -> binding.rbTrBl.id
                Orientation.RIGHT_LEFT -> binding.rbRl.id
                Orientation.BR_TL -> binding.rbBrTl.id
                Orientation.BOTTOM_TOP -> binding.rbBt.id
                Orientation.BL_TR -> binding.rbBlTr.id
                Orientation.LEFT_RIGHT -> binding.rbLr.id
                Orientation.TL_BR -> binding.rbTlBr.id
                else -> 0
            }
        )
    }

    fun setGradientRadius(radius: Int) {
        binding.sbGradientRadius.progress = radius
    }

    fun setGradientCenterX(x: Float) {
        binding.sbCenterX.progress = (x * 100).toInt()
    }

    fun setGradientCenterY(y: Float) {
        binding.sbCenterY.progress = (y * 100).toInt()
    }
}