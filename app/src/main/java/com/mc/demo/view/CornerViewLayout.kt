package com.mc.demo.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.lifecycle.MutableLiveData
import com.google.android.material.shape.CornerFamily
import com.mc.demo.R
import com.mc.demo.databinding.LayoutViewCornerBinding
import com.mc.fastkit.ext.dp2px

/**
 * CornerLayout
 * @author: MasterChan
 * @date: 2023-11-27 17:44
 */
class CornerViewLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val binding = LayoutViewCornerBinding.inflate(LayoutInflater.from(context), this, true)
    val cornerModelData = MutableLiveData<Int>()
    val cornerSizeData = MutableLiveData<Float>()

    init {
        binding.rgCorner.setOnCheckedChangeListener { _, checkedId ->
            cornerModelData.postValue(
                if (checkedId == R.id.rb_rounded) CornerFamily.ROUNDED else CornerFamily.CUT
            )
        }
        binding.seekbar.addOnChangeListener { _, value, _ ->
            cornerSizeData.postValue(dp2px(value))
        }
    }
}