package com.mc.demo.pages.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mc.demo.R
import com.mc.demo.databinding.FragmentDialogConfigBinding
import com.mc.fastkit.dialog.SimpleDialog
import com.mc.fastkit.dialog.XDialog
import com.mc.fastkit.dialog.animator.AnimatorItem
import com.mc.fastkit.dialog.animator.BgAnimator
import com.mc.fastkit.dialog.animator.BlurAnimator
import com.mc.fastkit.ext.singleClick
import com.mc.fastkit.ext.toast
import com.mc.fastkit.ui.BaseVBFragment
import com.mc.fastkit.widget.BaseViewHolder
import com.mc.fastkit.widget.XDivider
import com.skydoves.colorpickerview.ColorEnvelope
import com.skydoves.colorpickerview.ColorPickerDialog
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener

/**
 * ConfigFragment
 * @author: MasterChan
 * @date: 2024/3/6 14:11
 */
class ConfigFragment : BaseVBFragment<FragmentDialogConfigBinding>() {

    private val animatorItems = obtainAnimateList()
    private val animatorItemSelectedList = mutableListOf<AnimatorItem>()

    override fun onViewCreated(savedInstanceState: Bundle?) {
        XDivider().setSpacing(2)
            .setOrientation(RecyclerView.HORIZONTAL)
            .setDockingHorizontal(true)
            .setDrawEdge(true)
            .setDividerColor(Color.RED)
            .into(binding.recyclerView)
        XDivider().setSpacing(2)
            .setOrientation(RecyclerView.VERTICAL)
            .setDrawEdge(true)
            .setDividerColor(Color.RED)
            .into(binding.recyclerView)
        binding.recyclerView.layoutManager = GridLayoutManager(context, 3)
        binding.recyclerView.adapter = AnimateAdapter()
        binding.lvBg.singleClick {
            ColorPickerDialog.Builder(context)
                .setPositiveButton("确定", object : ColorEnvelopeListener {
                    override fun onColorSelected(envelope: ColorEnvelope, fromUser: Boolean) {
                        binding.lvBg.textView.text = "#${envelope.hexCode}"
                    }
                })
                .show()
        }
        binding.lvNavigationColor.singleClick {
            ColorPickerDialog.Builder(context)
                .setPositiveButton("确定", object : ColorEnvelopeListener {
                    override fun onColorSelected(envelope: ColorEnvelope, fromUser: Boolean) {
                        binding.lvNavigationColor.textView.text = "#${envelope.hexCode}"
                    }
                })
                .show()
        }
        binding.lvStatusBarColor.singleClick {
            ColorPickerDialog.Builder(context)
                .setPositiveButton("确定", object : ColorEnvelopeListener {
                    override fun onColorSelected(envelope: ColorEnvelope, fromUser: Boolean) {
                        binding.lvStatusBarColor.textView.text = "#${envelope.hexCode}"
                    }
                })
                .show()
        }
        binding.btnShow.singleClick {
            val dialog = SimpleDialog(context, R.layout.dialog_fragment_config)
            setDialog(dialog)
            dialog.show()
        }
    }

    private fun obtainAnimateList(): List<Triple<String, AnimatorItem, Boolean>> {
        val list = mutableListOf<Triple<String, AnimatorItem, Boolean>>()
        list.add(Triple("Alpha", AnimatorItem.Alpha, true))
        list.add(Triple("Translate.Left", AnimatorItem.Translate.Left, false))
        list.add(Triple("Translate.Top", AnimatorItem.Translate.Top, false))
        list.add(Triple("Translate.Right", AnimatorItem.Translate.Right, false))
        list.add(Triple("Translate.Bottom", AnimatorItem.Translate.Bottom, false))
        list.add(Triple("Scale.Center", AnimatorItem.Scale.Center, true))
        list.add(Triple("Scale.TopLeft", AnimatorItem.Scale.TopLeft, false))
        list.add(Triple("Scale.TopCenter", AnimatorItem.Scale.TopCenter, false))
        list.add(Triple("Scale.TopRight", AnimatorItem.Scale.TopRight, false))
        list.add(Triple("Scale.BottomLeft", AnimatorItem.Scale.BottomLeft, false))
        list.add(Triple("Scale.BottomCenter", AnimatorItem.Scale.BottomCenter, false))
        list.add(Triple("Scale.BottomRight", AnimatorItem.Scale.BottomRight, false))
        list.add(Triple("Scale.LeftCenter", AnimatorItem.Scale.LeftCenter, false))
        list.add(Triple("Scale.RightCenter", AnimatorItem.Scale.RightCenter, false))
        return list
    }

    private fun <T : XDialog<T>> setDialog(dialog: T) {
        val width = binding.etContentWidth.text.toString().toInt()
        val height = binding.etContentHeight.text.toString().toInt()
        dialog.setContentWidth(if (width == 0) null else width)
            .setContentHeight(if (height == 0) null else height)
            .setOffsetX(binding.sbOffsetX.value.toInt())
            .setOffsetY(binding.sbOffsetY.value.toInt())
            .setAnimatorDuration(binding.sbDuration.value.toLong())
            .setEnableAnimator(binding.cbEnableAnimator.isChecked)
            .setDismissOnBackPressed(binding.cbBackPress.isChecked)
            .setDismissOnTouchOutside(binding.cbDismissTouchOutside.isChecked)
            .setHideKeyboardOnTouchOutside(binding.cbHideKeyboardTouchOutside.isChecked)
            .setTouchThrough(binding.cbTouchThrough.isChecked)
            .setOnShowListener {
                toast("show")
            }
            .setOnDismissListener { toast("dismiss") }
            .setOnBackPressedListener {
                toast("点击返回")
                false
            }
            .setBgBlur(binding.cbBlur.isChecked)
            .setHideNavigationBar(binding.cbHideNavigation.isChecked)
            .setNavigationBarLight(binding.cbNavigationLight.isChecked)
            .setHideStatusBar(binding.cbHideStatusBar.isChecked)
            .setStatusBarLight(binding.cbStatusBarLight.isChecked)
            .apply {
                if (binding.rgGravity.checkedRadioButtonId != -1) {
                    setContentGravity(
                        when (binding.rgGravity.checkedRadioButtonId) {
                            R.id.rb_top -> Gravity.TOP
                            R.id.rb_center -> Gravity.CENTER
                            R.id.rb_bottom -> Gravity.BOTTOM
                            else -> Gravity.CENTER
                        }
                    )
                }
            }
            .apply {
                if (animatorItemSelectedList.isNotEmpty()) {
                    setContentAnimator(animatorItemSelectedList)
                }
            }
            .setBgColor(binding.lvBg.textView.text.toString())
            .setNavigationBarColor(binding.lvNavigationColor.textView.text.toString())
            .setStatusBarColor(binding.lvStatusBarColor.textView.text.toString())
            .setBgAnimator(if (binding.cbBlur.isChecked) BlurAnimator() else BgAnimator())
    }

    private inner class AnimateAdapter : RecyclerView.Adapter<BaseViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
            return BaseViewHolder(CheckBox(parent.context))
        }

        override fun getItemCount(): Int {
            return animatorItems.size
        }

        override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
            val checkBox = holder.itemView as CheckBox
            checkBox.text = animatorItems[position].first
            checkBox.textSize = 14f
            checkBox.setTextColor(Color.BLACK)
            checkBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    animatorItemSelectedList.add(animatorItems[position].second)
                } else {
                    animatorItemSelectedList.remove(animatorItems[position].second)
                }
            }
            checkBox.isChecked = animatorItems[position].third
        }
    }
}