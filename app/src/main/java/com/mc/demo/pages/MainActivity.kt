package com.mc.demo.pages

import android.os.Bundle
import android.view.View
import com.mc.demo.databinding.ActivityMainBinding
import com.mc.demo.pages.dialog.DialogActivity
import com.mc.fastkit.ext.bindSingleClicks
import com.mc.fastkit.ext.navigationBarHeight
import com.mc.fastkit.ext.screenHeight
import com.mc.fastkit.ext.screenRealHeight
import com.mc.fastkit.ext.screenWidth
import com.mc.fastkit.ext.startActivity
import com.mc.fastkit.ext.statusBarHeight
import com.mc.fastkit.log.LogUtils
import com.mc.fastkit.widget.ActivityStack

open class MainActivity : BaseFrameActivity<ActivityMainBinding>(),
    View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        printAppInfo()
        binding.bindSingleClicks(
            this, binding.btnTv, binding.btnLv, binding.btnIv, binding.btnDialog,
            binding.btnDivider, binding.btnForResult
        )
    }

    private fun printAppInfo() {
        LogUtils.d(
            """
            屏幕宽度：$screenWidth
            屏幕可用高度：$screenHeight
            屏幕实际高度：$screenRealHeight
            状态栏高度：$statusBarHeight
            导航栏高度：$navigationBarHeight
             """.trimIndent()
        )
        ActivityStack.instance.addOnAppStateChangedListener { isForeground, _ ->
            LogUtils.d("是否在前台运行：$isForeground")
        }
    }

    override fun onClick(view: View) {
        when (view) {
            binding.btnDialog -> startActivity(DialogActivity::class.java)
            binding.btnTv -> startActivity(ShapedTextViewActivity::class.java)
            binding.btnLv -> startActivity(ShapedLabelViewActivity::class.java)
            binding.btnIv -> startActivity(ShapedImageViewActivity::class.java)
            binding.btnDivider -> startActivity(DividerActivity::class.java)
            binding.btnForResult -> startActivity(ResultActivity::class.java)
            else -> {}
        }
    }
}