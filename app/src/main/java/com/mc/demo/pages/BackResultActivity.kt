package com.mc.demo.pages

import android.content.Intent
import android.os.Bundle
import com.mc.demo.databinding.ActivityBackResultBinding
import com.mc.fastkit.ext.singleClick

class BackResultActivity : BaseFrameActivity<ActivityBackResultBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.btn1.singleClick {
            setResult(RESULT_OK, Intent().putExtra("data", 123456))
            finish()
        }
        binding.btn2.singleClick {
            setResult(RESULT_OK, Intent().putExtra("data", "白日依山尽"))
            finish()
        }
        binding.btn3.singleClick {
            finish()
        }
    }
}