package com.mc.demo.pages

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.mc.demo.R
import com.mc.demo.databinding.ActivityDividerBinding
import com.mc.fastkit.ext.dp2pxi
import com.mc.fastkit.ext.setSize
import com.mc.fastkit.ext.singleClick
import com.mc.fastkit.widget.BaseViewHolder
import com.mc.fastkit.widget.XDivider

/**
 * DividerActivity
 * @author: MasterChan
 * @date: 2023-12-11 17:44
 */
class DividerActivity : BaseFrameActivity<ActivityDividerBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.adapter = Adapter()

        binding.rgRvOrientation.setOnCheckedChangeListener { _, checkedId ->
            val orientation = if (checkedId == R.id.rb_rv_orientation_h) {
                RecyclerView.HORIZONTAL
            } else {
                RecyclerView.VERTICAL
            }
            when (val layoutManager = binding.recyclerView.layoutManager) {
                is GridLayoutManager -> layoutManager.orientation = orientation
                is LinearLayoutManager -> layoutManager.orientation = orientation
                is StaggeredGridLayoutManager -> layoutManager.orientation = orientation
            }
            binding.recyclerView.adapter?.notifyDataSetChanged()
        }
        binding.rgLayoutManager.setOnCheckedChangeListener { _, checkedId ->
            for (i in 0 until binding.recyclerView.itemDecorationCount) {
                binding.recyclerView.removeItemDecorationAt(0)
            }
            binding.rgDividerOrientation.check(R.id.rb_divider_orientation_h)
            binding.sbSpacing.value = 0f
            binding.rgDividerColor.check(R.id.rb_divider_blue)
            binding.rgDrawFirst.check(R.id.rb_drawFirst_no)
            binding.rgDrawLast.check(R.id.rb_drawLast_no)
            binding.sbStartPadding.value = 0f
            binding.sbEndPadding.value = 0f
            binding.sbTopPadding.value = 0f
            binding.sbBottomPadding.value = 0f
            val orientation = if (binding.rgRvOrientation.checkedRadioButtonId == R.id.rb_rv_orientation_h) {
                RecyclerView.HORIZONTAL
            } else {
                RecyclerView.VERTICAL
            }
            when (checkedId) {
                R.id.rb_linear -> {
                    binding.recyclerView.layoutManager = LinearLayoutManager(
                        context, orientation, false
                    )
                }

                R.id.rb_grid -> {
                    binding.recyclerView.layoutManager = GridLayoutManager(
                        context, 3, orientation, false
                    )
                }

                R.id.rb_staggered -> {
                    binding.recyclerView.layoutManager = StaggeredGridLayoutManager(3, orientation)
                }

                else -> {}
            }
            binding.recyclerView.adapter?.notifyDataSetChanged()
        }
        binding.btnAdd.singleClick {
            val orientation = if (binding.rgDividerOrientation.checkedRadioButtonId == R.id.rb_divider_orientation_h) {
                RecyclerView.HORIZONTAL
            } else {
                RecyclerView.VERTICAL
            }
            val dividerWidth = binding.sbSpacing.value.toInt()
            val dividerColor = if (binding.rgDividerColor.checkedRadioButtonId == R.id.rb_divider_blue) {
                Color.BLUE
            } else {
                Color.RED
            }
            val includeMargin = binding.rgIncludeMargin.checkedRadioButtonId == R.id.rb_include_margin_yes
            val isDrawFirst = binding.rgDrawFirst.checkedRadioButtonId == R.id.rb_drawFirst_yes
            val isDrawLast = binding.rgDrawLast.checkedRadioButtonId == R.id.rb_drawLast_yes
            val startPadding = binding.sbStartPadding.value.toInt()
            val topPadding = binding.sbTopPadding.value.toInt()
            val endPadding = binding.sbEndPadding.value.toInt()
            val bottomPadding = binding.sbBottomPadding.value.toInt()
            XDivider().setOrientation(orientation)
                .setSpacing(dividerWidth)
                .setDividerColor(dividerColor)
                .setIncludeMargin(includeMargin)
                .setDrawFirst(isDrawFirst)
                .setDrawLast(isDrawLast)
                .setStartPadding(startPadding)
                .setTopPadding(topPadding)
                .setEndPadding(endPadding)
                .setBottomPadding(bottomPadding)
                .into(binding.recyclerView)
        }
        binding.btnRemove.singleClick {
            if (binding.recyclerView.itemDecorationCount > 0) {
                binding.recyclerView.removeItemDecorationAt(
                    binding.recyclerView.itemDecorationCount - 1
                )
            }
        }
    }

    private inner class Adapter : RecyclerView.Adapter<BaseViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
            return BaseViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_divider, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return 30
        }

        override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
            holder.itemView.findViewById<TextView>(R.id.tv_text).text = position.toString()
            when (val layoutManager = binding.recyclerView.layoutManager) {
                is GridLayoutManager -> {
                    if (layoutManager.orientation == RecyclerView.VERTICAL) {
                        holder.itemView.setSize(-1, dp2pxi(100))
                    } else {
                        holder.itemView.setSize(dp2pxi(100), -1)
                    }
                }

                is LinearLayoutManager -> {
                    if (layoutManager.orientation == RecyclerView.VERTICAL) {
                        holder.itemView.setSize(-1, dp2pxi(100))
                    } else {
                        holder.itemView.setSize(dp2pxi(100), dp2pxi(100))
                    }
                }
            }
        }
    }
}