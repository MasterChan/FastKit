package com.mc.demo.pages

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.mc.fastkit.log.LogUtils
import com.mc.fastkit.ui.BaseViewModel

/**
 *
 * @author: MasterChan
 * @date: 2024-04-08 14:59
 */
class ResultViewModel : BaseViewModel() {

    var count = MutableLiveData(10)

    override fun onCreate(owner: LifecycleOwner) {
        LogUtils.i(owner)
        LogUtils.d("${javaClass.simpleName}:onCreate")
    }

    override fun onResume(owner: LifecycleOwner) {
        LogUtils.d("${javaClass.simpleName}:onResume")
    }

    override fun onPause(owner: LifecycleOwner) {
        LogUtils.d("${javaClass.simpleName}:onPause")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        LogUtils.d("${javaClass.simpleName}:onDestroy")
    }
}