package com.mc.demo.pages.dialog

import android.content.Context
import com.mc.demo.R
import com.mc.fastkit.dialog.BottomSheetDialog

/**
 * SheetDialog2
 * @author: MasterChan
 * @date: 2024-03-08 17:31
 */
class SheetDialog2(context: Context) : BottomSheetDialog<SheetDialog2>(context) {

    override fun onCreate() {
        setContentView(R.layout.dialog_sheet2)
    }
}