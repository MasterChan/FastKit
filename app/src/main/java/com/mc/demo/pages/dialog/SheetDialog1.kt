package com.mc.demo.pages.dialog

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mc.demo.databinding.DialogSheet1Binding
import com.mc.fastkit.dialog.BindingDialog
import com.mc.fastkit.ext.singleClick
import com.mc.fastkit.ext.toast
import com.mc.fastkit.widget.BaseViewHolder
import com.mc.fastkit.widget.XDivider

/**
 * 只有RecyclerView的Dialog
 * @author: MasterChan
 * @date: 2024/3/6 16:24
 */
class SheetDialog1(context: Context) : BindingDialog<DialogSheet1Binding, SheetDialog1>(context) {

    override fun onCreated() {
        isStatusBarLight = true
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        XDivider().setSpacing(1)
            .setDividerColor(Color.GRAY)
            .setDrawFirst(true)
            .into(binding.recyclerView)
        binding.recyclerView.adapter = object : RecyclerView.Adapter<BaseViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
                return BaseViewHolder(
                    LayoutInflater.from(context)
                        .inflate(android.R.layout.simple_list_item_1, parent, false)
                )
            }

            override fun getItemCount(): Int {
                return 50
            }

            override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                holder.setText(android.R.id.text1, position.toString())
                holder.itemView.singleClick {
                    toast(position.toString())
                }
            }
        }
    }
}