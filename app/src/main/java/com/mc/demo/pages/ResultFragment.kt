package com.mc.demo.pages

import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.LogUtils
import com.mc.demo.databinding.FragmentResultBinding
import com.mc.fastkit.ext.singleClick
import com.mc.fastkit.ui.BaseVMFragment

/**
 * ResultFragment
 * @author: MasterChan
 * @date: 2024-02-22 16:31
 */
class ResultFragment : BaseVMFragment<FragmentResultBinding, ResultViewModel>() {

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.tvText.text = requireArguments().getInt("position").toString()
        binding.btnNext.singleClick {
            viewModel.count.postValue(viewModel.count.value!! + 1)
            startActivityForResult(com.mc.demo.pages.BackResultActivity::class.java) {
                binding.tvResult.text = "${it.data?.extras?.get("data").toString()}"
            }
        }
        viewModel.count.observe(viewLifecycleOwner) {
            LogUtils.d("${javaClass.simpleName}:$it")
        }
    }
}