package com.mc.demo.pages.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.R
import com.mc.fastkit.dialog.BottomSheetDialog
import com.mc.fastkit.ext.setSize
import com.mc.fastkit.widget.BaseViewHolder

/**
 * SheetDialog4
 * @author: MasterChan
 * @date: 2024-3-10 17:34
 */
class SheetDialog4(context: Context) : BottomSheetDialog<SheetDialog4>(context) {

    override fun onCreate() {
        setContentView(R.layout.dialog_sheet4)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager2>(R.id.viewPager)
        viewPager.adapter = object : Adapter<BaseViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.dialog_sheet1, parent, false)
                view.setSize(-1, -1)
                return BaseViewHolder(view)
            }

            override fun getItemCount(): Int {
                return 10
            }

            override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                val recyclerView = holder.getView<RecyclerView>(R.id.recyclerView)!!
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = object : Adapter<BaseViewHolder>() {
                    override fun onCreateViewHolder(
                        parent: ViewGroup,
                        viewType: Int
                    ): BaseViewHolder {
                        return BaseViewHolder(
                            LayoutInflater.from(context)
                                .inflate(android.R.layout.simple_list_item_1, parent, false)
                        )
                    }

                    override fun getItemCount(): Int {
                        return 50
                    }

                    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                        holder.setText(android.R.id.text1, position.toString())
                    }
                }
            }
        }
        TabLayoutMediator(tabLayout, viewPager) { tab, index ->
            tab.setText(index.toString())
        }.attach()
    }
}