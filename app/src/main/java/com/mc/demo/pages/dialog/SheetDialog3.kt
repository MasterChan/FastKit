package com.mc.demo.pages.dialog

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.R
import com.mc.fastkit.dialog.BottomSheetDialog
import com.mc.fastkit.ext.cast
import com.mc.fastkit.ext.setSize
import com.mc.fastkit.widget.BaseViewHolder

/**
 * SheetDialog3
 * @author: MasterChan
 * @date: 2024-3-10 17:34
 */
class SheetDialog3(context: Context) : BottomSheetDialog<SheetDialog3>(context) {

    override fun onCreate() {
        setContentView(R.layout.dialog_sheet4)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager2>(R.id.viewPager)
        viewPager.adapter = object : Adapter<BaseViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
                val view = TextView(context)
                view.gravity = Gravity.CENTER
                view.textSize = 18f
                view.setTextColor(Color.BLACK)
                view.setSize(-1, -1)
                return BaseViewHolder(view)
            }

            override fun getItemCount(): Int {
                return 10
            }

            override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                holder.itemView.cast<TextView>().text = position.toString()
            }
        }
        TabLayoutMediator(tabLayout, viewPager) { tab, index ->
            tab.setText(index.toString())
        }.attach()
    }
}