package com.mc.demo.pages

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.blankj.utilcode.util.LogUtils
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.databinding.ActivityResultBinding
import com.mc.fastkit.ext.singleClick
import com.mc.fastkit.ui.BaseVMActivity
import com.mc.fastkit.utils.FragmentUtils

class ResultActivity : BaseVMActivity<ActivityResultBinding, ResultViewModel>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewPager.offscreenPageLimit = 1
        binding.viewPager.adapter = Adapter()
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = "第${position}页"
        }.attach()
        binding.btn.singleClick {
            viewModel.count.postValue(viewModel.count.value!! + 1)
            startActivityForResult(BackResultActivity::class.java) {
                binding.tvText.text = it.data?.extras?.get("data")?.toString()
            }
        }
        viewModel.count.observe(this) {
            LogUtils.d("${javaClass.simpleName}:$it")
        }
    }

    private inner class Adapter : FragmentStateAdapter(this) {
        override fun getItemCount(): Int {
            return 20
        }

        override fun createFragment(position: Int): Fragment {
            return FragmentUtils.newInstance<ResultFragment> { putInt("position", position) }
        }
    }
}