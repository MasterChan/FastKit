package com.mc.demo.pages.dialog

import android.os.Bundle
import android.view.View
import com.mc.demo.databinding.FragmentDialogCustomBinding
import com.mc.fastkit.ext.bindClicks
import com.mc.fastkit.ui.BaseVBFragment

/**
 * CustomFragment
 * @author: MasterChan
 * @date: 2024/3/6 14:54
 */
class CustomFragment : BaseVBFragment<FragmentDialogCustomBinding>(), View.OnClickListener {

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.bindClicks(
            this, binding.btn1, binding.btn2, binding.btn3, binding.btn4, binding.btn5, binding.btn6
        )
    }

    override fun onClick(v: View) {
        when (v) {
            binding.btn1 -> SheetDialog1(context).show()
            binding.btn2 -> SheetDialog2(context).setNextStateRatio(0.1f).show()
            binding.btn3 -> SheetDialog3(context).show()
            binding.btn4 -> SheetDialog4(context).show()
            binding.btn5 -> SheetDialog5(context).show()
            binding.btn6 -> SheetDialog6(context, this).show()
        }
    }
}