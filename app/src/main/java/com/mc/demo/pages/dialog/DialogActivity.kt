package com.mc.demo.pages.dialog

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.databinding.ActivityDialogBinding
import com.mc.demo.pages.BaseFrameActivity
import com.mc.fastkit.utils.FragmentUtils

/**
 * DialogActivity
 * @author: MasterChan
 * @date: 2023-12-28 14:47
 */
class DialogActivity : BaseFrameActivity<ActivityDialogBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewPager.adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int {
                return 2
            }

            override fun createFragment(position: Int): Fragment {
                return if (position == 0) {
                    FragmentUtils.newInstance<ConfigFragment> { }
                } else {
                    FragmentUtils.newInstance<CustomFragment> { }
                }
            }
        }
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, index ->
            tab.setText(if (index == 0) "配置" else "自定义")
        }.attach()
    }
}