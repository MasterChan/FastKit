package com.mc.demo.pages.dialog

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.R
import com.mc.fastkit.dialog.BottomSheetDialog

/**
 * SheetDialog6
 * @author: MasterChan
 * @date: 2024-3-10 17:34
 */
class SheetDialog6(context: Context, private val fragment: Fragment) :
    BottomSheetDialog<SheetDialog6>(context) {

    private val childFragments = arrayOf(ChildFragment1(), ChildFragment1())

    override fun onCreate() {
        setContentView(R.layout.dialog_sheet4)
        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager2>(R.id.viewPager)
        viewPager.adapter = object : FragmentStateAdapter(fragment) {
            override fun getItemCount(): Int {
                return childFragments.size
            }

            override fun createFragment(position: Int): Fragment {
                return childFragments[position]
            }
        }
        TabLayoutMediator(tabLayout, viewPager) { tab, index ->
            tab.setText(index.toString())
        }.attach()
    }

}