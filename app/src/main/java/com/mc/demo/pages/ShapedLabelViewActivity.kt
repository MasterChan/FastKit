package com.mc.demo.pages

import android.os.Bundle
import android.view.Gravity
import com.mc.demo.R
import com.mc.fastkit.ui.BaseVBActivity
import com.mc.demo.databinding.ActivityShapedLabelViewBinding
import com.mc.fastkit.ext.dp2pxi
import com.mc.fastkit.ext.setHeight
import com.mc.fastkit.ext.setOnSingleClickListener
import com.mc.fastkit.ext.setWidth
import com.mc.fastkit.ext.toast
import com.mc.fastkit.view.shape.ShapedLabelView
import com.mc.fastkit.view.shape.StateType

class ShapedLabelViewActivity : BaseVBActivity<ActivityShapedLabelViewBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.labelView.setOnSingleClickListener {
            toast("ImageTextView")
        }
        binding.tvAddText.setOnSingleClickListener {
            val lastText = binding.labelView.textView.text
            binding.labelView.textView.text = "$lastText,白日依山尽，黄河入海流"
        }
        //view宽度
        binding.rgWidth.setOnCheckedChangeListener { _, checkedId ->
            binding.labelView.setWidth(
                if (checkedId == R.id.rb_width_wrap_content) -2 else -1
            )
        }
        //view高度
        binding.rgHeight.setOnCheckedChangeListener { _, checkedId ->
            binding.labelView.setHeight(
                if (checkedId == R.id.rb_height_wrap_content) -2 else dp2pxi(200)
            )
        }
        //图片Gravity
        binding.rgImageGravity.setOnCheckedChangeListener { _, checkedId ->
            binding.labelView.setImageGravity(
                when (checkedId) {
                    R.id.rb_image_gravity_start -> ShapedLabelView.ImageGravity.START
                    R.id.rb_image_gravity_top -> ShapedLabelView.ImageGravity.TOP
                    R.id.rb_image_gravity_end -> ShapedLabelView.ImageGravity.END
                    R.id.rb_image_gravity_bottom -> ShapedLabelView.ImageGravity.BOTTOM
                    R.id.rb_image_gravity_center -> ShapedLabelView.ImageGravity.CENTER
                    else -> ShapedLabelView.ImageGravity.START
                }
            )
        }
        //图片宽度
        binding.imageWidth.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageWidth(value.toInt())
        }
        //图片高度
        binding.imageHeight.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageHeight(value.toInt())
        }
        //图片间距
        binding.imageSpacing.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageSpacing(value.toInt())
        }
        //imageMarginStart
        binding.sbMarginStart.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageMarginStart(dp2pxi(value))
        }
        //imageMarginTop
        binding.sbMarginTop.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageMarginTop(dp2pxi(value))
        }
        //imageMarginEnd
        binding.sbMarginEnd.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageMarginEnd(dp2pxi(value))
        }
        //imageMarginBottom
        binding.sbMarginBottom.addOnChangeListener { _, value, _ ->
            binding.labelView.setImageMarginBottom(dp2pxi(value))
        }
        //文字宽度
        binding.textWidth.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextWidth(value.toInt())
        }
        //文字高度
        binding.textHeight.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextHeight(value.toInt())
        }
        //文字可滚动
        binding.cbTextScrollable.setOnCheckedChangeListener { _, isChecked ->
            binding.labelView.setTextScrollAble(isChecked)
        }
        //文字位置
        binding.rgTextGravity.setOnCheckedChangeListener { _, checkedId ->
            binding.labelView.setTextGravity(
                when (checkedId) {
                    R.id.rb_text_gravity_start -> Gravity.START
                    R.id.rb_text_gravity_top -> Gravity.TOP
                    R.id.rb_text_gravity_end -> Gravity.END
                    R.id.rb_text_gravity_bottom -> Gravity.BOTTOM
                    R.id.rb_text_gravity_center -> Gravity.CENTER
                    R.id.rb_text_gravity_center_horizontal -> Gravity.CENTER_HORIZONTAL
                    R.id.rb_text_gravity_center_vertical -> Gravity.CENTER_VERTICAL
                    else -> Gravity.START
                }
            )
        }
        //textPaddingStart
        binding.sbPaddingStart.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextStartPadding(dp2pxi(value))
        }
        //textPaddingTop
        binding.sbPaddingTop.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextTopPadding(dp2pxi(value))
        }
        //textPaddingEnd
        binding.sbPaddingEnd.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextEndPadding(dp2pxi(value))
        }
        //textPaddingBottom
        binding.sbPaddingBottom.addOnChangeListener { _, value, _ ->
            binding.labelView.setTextBottomPadding(dp2pxi(value))
        }
        binding.rgStateType.setOnCheckedChangeListener { _, checkedId ->
            binding.labelView.setStateType(
                when (checkedId) {
                    R.id.rb_state_none -> StateType.NONE
                    R.id.rb_state_pressed -> StateType.PRESSED
                    R.id.rb_state_selected -> StateType.SELECTED
                    R.id.rb_state_checked -> StateType.CHECKED
                    else -> StateType.NONE
                }
            ).setup()
        }
        binding.rgSwitchState.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.rb_none -> {
                    binding.labelView.isEnabled = true
                    binding.labelView.isSelected = false
                }

                R.id.rb_selected -> {
                    binding.labelView.isEnabled = true
                    binding.labelView.isSelected = true
                }

                R.id.rb_checked -> {}
                R.id.rb_disable -> {
                    binding.labelView.isEnabled = false
                    binding.labelView.isSelected = false
                }
            }
        }
    }
}