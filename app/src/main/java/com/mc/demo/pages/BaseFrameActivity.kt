package com.mc.demo.pages

import androidx.viewbinding.ViewBinding
import com.mc.fastkit.ui.BaseVBActivity

/**
 * BaseFrameActivity
 * @author: MasterChan
 * @date: 2023-03-18 23:25
 */
open class BaseFrameActivity<T : ViewBinding> : BaseVBActivity<T>()