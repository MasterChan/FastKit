package com.mc.demo.pages

import android.os.Bundle
import com.mc.demo.databinding.ActivityShapedTextViewBinding
import com.mc.demo.view.GradientView
import com.mc.fastkit.ext.setOnSingleClickListener
import com.mc.fastkit.utils.ToastUtils
import com.mc.fastkit.view.shape.GradientAttrs
import com.mc.fastkit.view.shape.StateType

class ShapedTextViewActivity : BaseFrameActivity<ActivityShapedTextViewBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //圆形
        binding.rgIsRounded.setOnCheckedChangeListener { _, checkedId ->
            binding.shapedView.setRounded(checkedId == binding.rbIsRoundedYes.id).setup()
        }
        //水波纹
        binding.rgUseRipple.setOnCheckedChangeListener { _, checkedId ->
            binding.shapedView.setUseRipple(checkedId == binding.rbRippleYes.id).setup()
        }
        //状态类型
        binding.rgStateType.setOnCheckedChangeListener { _, checkedId ->
            binding.shapedView.setStateType(
                when (checkedId) {
                    binding.rbStateNone.id -> StateType.NONE
                    binding.rbStatePressed.id -> StateType.PRESSED
                    binding.rbStateSelected.id -> StateType.SELECTED
                    binding.rbStateChecked.id -> StateType.CHECKED
                    else -> StateType.NONE
                }
            ).setup()
        }
        //切换状态
        binding.rgSwitchState.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                binding.rbNone.id -> {
                    binding.shapedView.isEnabled = true
                    binding.shapedView.isSelected = false
                }

                binding.rbSelected.id -> {
                    binding.shapedView.isEnabled = true
                    binding.shapedView.isSelected = true
                }

                binding.rbChecked.id -> {}
                binding.rbDisable.id -> {
                    binding.shapedView.isEnabled = false
                    binding.shapedView.isSelected = false
                }
            }
        }
        setNormalAttrs()
        setAttrs(binding.stateView, binding.shapedView.stateAttrs)
        setAttrs(binding.disableView, binding.shapedView.disableAttrs)
        binding.shapedView.setOnSingleClickListener {
            ToastUtils.show("ShapeView")
        }
    }

    private fun setNormalAttrs() {
        binding.normalView.radiusData.observe(this) {
            binding.shapedView.normalAttrs.setRadius(it.toFloat()).setup()
            binding.stateView.setRadius(it)
            binding.disableView.setRadius(it)
        }
        binding.normalView.strokeWidthData.observe(this) {
            binding.shapedView.normalAttrs.setStrokeWidth(it).setup()
            binding.stateView.setStrokeWidth(it)
            binding.disableView.setStrokeWidth(it)
        }
        binding.normalView.dashWidthData.observe(this) {
            binding.shapedView.normalAttrs.setDashWidth(it.toFloat()).setup()
            binding.stateView.setDashWidth(it)
            binding.disableView.setDashWidth(it)
        }
        binding.normalView.dashGapWidthData.observe(this) {
            binding.shapedView.normalAttrs.setDashGap(it.toFloat()).setup()
            binding.stateView.setDashGapWidth(it)
            binding.disableView.setDashGapWidth(it)
        }
        binding.normalView.gradientTypeData.observe(this) {
            binding.shapedView.normalAttrs.setGradientType(it).setup()
            binding.stateView.setGradientType(it)
            binding.disableView.setGradientType(it)
        }
        binding.normalView.gradientOrientationData.observe(this) {
            binding.shapedView.normalAttrs.setGradientOrientation(it).setup()
            binding.stateView.setGradientOrientation(it)
            binding.disableView.setGradientOrientation(it)
        }
        binding.normalView.gradientRadiusData.observe(this) {
            binding.shapedView.normalAttrs.setGradientRadius(it.toFloat()).setup()
            binding.stateView.setGradientRadius(it)
            binding.disableView.setGradientRadius(it)
        }
        binding.normalView.gradientCenterXData.observe(this) {
            binding.shapedView.normalAttrs.setGradientCenterX(it.toFloat()).setup()
            binding.stateView.setGradientCenterX(it)
            binding.disableView.setGradientCenterX(it)
        }
        binding.normalView.gradientCenterYData.observe(this) {
            binding.shapedView.normalAttrs.setGradientCenterY(it.toFloat()).setup()
            binding.stateView.setGradientCenterY(it)
            binding.disableView.setGradientCenterY(it)
        }
    }

    private fun setAttrs(view: GradientView, attrs: GradientAttrs) {
        view.radiusData.observe(this) { attrs.setRadius(it.toFloat()).setup() }
        view.strokeWidthData.observe(this) { attrs.setStrokeWidth(it).setup() }
        view.dashWidthData.observe(this) { attrs.setDashWidth(it.toFloat()).setup() }
        view.dashGapWidthData.observe(this) { attrs.setDashGap(it.toFloat()).setup() }
        view.gradientTypeData.observe(this) { attrs.setGradientType(it).setup() }
        view.gradientOrientationData.observe(this) { attrs.setGradientOrientation(it).setup() }
        view.gradientRadiusData.observe(this) { attrs.setGradientRadius(it.toFloat()).setup() }
        view.gradientCenterXData.observe(this) { attrs.setGradientCenterX(it).setup() }
        view.gradientCenterYData.observe(this) { attrs.setGradientCenterY(it).setup() }
    }
}