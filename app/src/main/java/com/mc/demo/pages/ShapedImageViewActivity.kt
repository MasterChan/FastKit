package com.mc.demo.pages

import android.os.Bundle
import android.widget.ImageView
import androidx.core.view.setPadding
import com.google.android.material.shape.CutCornerTreatment
import com.google.android.material.shape.RoundedCornerTreatment
import com.mc.demo.R
import com.mc.demo.databinding.ActivityShapedImageViewBinding
import com.mc.fastkit.ext.setOnSingleClickListener
import com.mc.fastkit.ui.BaseVBActivity

class ShapedImageViewActivity : BaseVBActivity<ActivityShapedImageViewBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.ivImage.setOnSingleClickListener { it.isSelected = !it.isSelected }
        //设置ScaleType
        binding.rgScaleType.setOnCheckedChangeListener { _, checkedId ->
            binding.ivImage.scaleType = when (checkedId) {
                R.id.rb_fit_center -> ImageView.ScaleType.FIT_CENTER
                R.id.rb_center_crop -> ImageView.ScaleType.CENTER_CROP
                R.id.rb_fit_xy -> ImageView.ScaleType.FIT_XY
                else -> ImageView.ScaleType.FIT_CENTER
            }
        }
        //设置rounded
        binding.rgIsRounded.setOnCheckedChangeListener { _, checkedId ->
            binding.ivImage.setRounded(checkedId == R.id.rb_is_rounded_yes).setup()
        }
        //strokeWidth
        binding.sbStrokeWidth.addOnChangeListener { _, value, _ ->
            binding.ivImage.apply { setPadding(value.toInt()) }.setStrokeWidth(value).setup()
        }
        //dash
        binding.sbDashWidth.addOnChangeListener { _, value, _ ->
            binding.ivImage.setDash(value, binding.sbDashGap.value).setup()
        }
        //dash
        binding.sbDashGap.addOnChangeListener { _, value, _ ->
            binding.ivImage.setDash(binding.sbDashWidth.value, value).setup()
        }
        //TopLeftCorner
        binding.topLeftCornerLayout.cornerModelData.observe(this) {
            binding.ivImage.setTopLeftCornerTreatment(
                if (it == 0) RoundedCornerTreatment() else CutCornerTreatment()
            ).setup()
        }
        binding.topLeftCornerLayout.cornerSizeData.observe(this) {
            binding.ivImage.setCornerSizeTopLeft(it).setup()
        }
        //TopRightCorner
        binding.topRightCornerLayout.cornerModelData.observe(this) {
            binding.ivImage.setTopRightCornerTreatment(
                if (it == 0) RoundedCornerTreatment() else CutCornerTreatment()
            ).setup()
        }
        binding.topRightCornerLayout.cornerSizeData.observe(this) {
            binding.ivImage.setCornerSizeTopRight(it).setup()
        }
        //BottomLeftCorner
        binding.bottomLeftCornerLayout.cornerModelData.observe(this) {
            binding.ivImage.setBottomLeftCornerTreatment(
                if (it == 0) RoundedCornerTreatment() else CutCornerTreatment()
            ).setup()
        }
        binding.bottomLeftCornerLayout.cornerSizeData.observe(this) {
            binding.ivImage.setCornerSizeBottomLeft(it).setup()
        }
        //BottomRightCorner
        binding.bottomRightCornerLayout.cornerModelData.observe(this) {
            binding.ivImage.setBottomRightCornerTreatment(
                if (it == 0) RoundedCornerTreatment() else CutCornerTreatment()
            ).setup()
        }
        binding.bottomRightCornerLayout.cornerSizeData.observe(this) {
            binding.ivImage.setCornerSizeBottomRight(it).setup()
        }
    }
}