package com.mc.demo.pages.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayoutMediator
import com.mc.demo.R
import com.mc.demo.databinding.FragmentDialogCustomChild1Binding
import com.mc.fastkit.ext.cast
import com.mc.fastkit.ext.setSize
import com.mc.fastkit.ui.BaseVBFragment
import com.mc.fastkit.widget.BaseViewHolder

/**
 * ChildFragment1
 * @author: MasterChan
 * @date: 2024-03-11 16:29
 */
class ChildFragment1 : BaseVBFragment<FragmentDialogCustomChild1Binding>() {

    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.viewPager.adapter = object : RecyclerView.Adapter<BaseViewHolder>() {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
                val itemView = LayoutInflater.from(context)
                    .inflate(R.layout.dialog_sheet1, parent, false)
                itemView.setSize(-1, -1)
                return BaseViewHolder(itemView)
            }

            override fun getItemCount(): Int {
                return 10
            }

            override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                val recyclerView = holder.getView<RecyclerView>(R.id.recyclerView)!!
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = object : RecyclerView.Adapter<BaseViewHolder>() {
                    override fun onCreateViewHolder(
                        parent: ViewGroup,
                        viewType: Int
                    ): BaseViewHolder {
                        return BaseViewHolder(
                            LayoutInflater.from(context)
                                .inflate(android.R.layout.simple_list_item_1, parent, false)
                        )
                    }

                    override fun getItemCount(): Int {
                        return 40
                    }

                    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
                        holder.itemView.cast<TextView>().text = position.toString()
                    }
                }
            }
        }
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, index ->
            tab.setText(index.toString())
        }.attach()
    }
}