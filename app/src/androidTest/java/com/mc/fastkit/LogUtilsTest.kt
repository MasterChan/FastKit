package com.mc.fastkit

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mc.fastkit.log.LogUtils
import org.junit.Test
import org.junit.runner.RunWith

/**
 *
 * @author: MasterChan
 * @date: 2023-02-24 21:18
 */
@RunWith(AndroidJUnit4::class)
class LogUtilsTest {

    @Test
    fun printLog() {
        (0..10000).forEach { LogUtils.d(it.toString()) }
    }
}