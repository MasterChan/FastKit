package com.mc.fastkit.log

import androidx.annotation.IntDef

/**
 * 打印等级，与[android.util.Log]的打印等级对应
 * @author: MasterChan
 * @date: 2022-05-29 13:20
 */
@IntDef(
    Priority.VERBOSE, Priority.DEBUG, Priority.INFO, Priority.WARN, Priority.ERROR, Priority.ASSERT
)
@Retention(AnnotationRetention.SOURCE)
annotation class Priority {
    companion object {
        const val VERBOSE = 2
        const val DEBUG = 3
        const val INFO = 4
        const val WARN = 5
        const val ERROR = 6
        const val ASSERT = 7
    }
}