package com.mc.fastkit.log

import com.mc.fastkit.R
import com.mc.fastkit.ext.app

/**
 * 日志文件管理接口
 * @author: MasterChan
 * @date: 2023-02-21 09:40:32
 */
interface ILogManager {

    companion object {
        var config = Config()
    }

    fun setConfig(value: Config) {
        config = value
    }

    fun getConfig(): Config {
        return config
    }

    fun save(log: String)

    data class Config(
        var logDir: String = "${app.filesDir.absolutePath}/${app.getString(R.string.app_name)}/Log",
        var extension: String = "log",
        var maxFileSize: Int = 1024 * 1000,
        var saveDays: Int = 10
    )
}