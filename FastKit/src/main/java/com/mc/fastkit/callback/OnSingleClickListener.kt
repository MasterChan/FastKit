package com.mc.fastkit.callback

import android.view.View

/**
 * 防重复点击事件
 * @author: MasterChan
 * @date: 2023-03-18 22:52
 */
abstract class OnSingleClickListener(private val delay: Long = 500) : View.OnClickListener {
    override fun onClick(v: View) {
        val tagKey = Int.MAX_VALUE - 1001
        val tag = v.getTag(tagKey)
        if (tag != null && System.currentTimeMillis() - (tag as Long) < delay) {
            onSingleClick(v, true)
        } else {
            onSingleClick(v, false)
        }
        v.setTag(tagKey, System.currentTimeMillis())
    }

    open fun onSingleClick(view: View, isFast: Boolean) {}
}