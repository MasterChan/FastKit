package com.mc.fastkit.callback

import android.widget.SeekBar

/**
 * SimpleSeekBarChangeListener
 * @author: MasterChan
 * @date: 2023-03-19 14:55
 */
open class SimpleSeekBarChangeListener : SeekBar.OnSeekBarChangeListener {
    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {}
    override fun onStartTrackingTouch(seekBar: SeekBar) {}
    override fun onStopTrackingTouch(seekBar: SeekBar) {}
}