package com.mc.fastkit.ui

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.ViewModel

/**
 * BaseViewModel
 * @author: MasterChan
 * @date: 2024-04-08 14:24
 */
open class BaseViewModel : ViewModel(), DefaultLifecycleObserver