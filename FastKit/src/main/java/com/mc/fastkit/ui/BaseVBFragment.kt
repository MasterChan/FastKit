package com.mc.fastkit.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType

/**
 * BaseVBFragment
 * @author: MasterChan
 * @date: 2023-02-25 16:11
 */
abstract class BaseVBFragment<T : ViewBinding> : BaseFragment() {

    protected val binding: T by lazy { getViewBinding() }

    override fun getContentView(inflater: LayoutInflater, container: ViewGroup?): View? {
        return binding.root
    }

    @Suppress("UNCHECKED_CAST")
    protected open fun getViewBinding(): T {
        var superClass = javaClass.genericSuperclass
        while (superClass !is ParameterizedType) {
            superClass = (superClass as Class<*>).genericSuperclass
        }
        return (superClass.actualTypeArguments[0] as Class<*>).getMethod(
            "inflate", LayoutInflater::class.java
        ).invoke(null, layoutInflater) as T
    }
}