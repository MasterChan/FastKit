package com.mc.fastkit.ui

import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import java.lang.reflect.ParameterizedType

/**
 * 在[BaseActivity]基础上使用[ViewBinding]
 * @author: MasterChan
 * @date: 2023-3-11 22:21
 */
open class BaseVBActivity<T : ViewBinding> : BaseActivity() {

    protected val binding: T by lazy { getViewBinding() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    @Suppress("UNCHECKED_CAST")
    protected open fun getViewBinding(): T {
        var superClass = javaClass.genericSuperclass
        while (superClass !is ParameterizedType) {
            superClass = (superClass as Class<*>).genericSuperclass
        }
        return (superClass.actualTypeArguments[0] as Class<*>).getMethod(
            "inflate", LayoutInflater::class.java
        ).invoke(null, layoutInflater) as T
    }
}