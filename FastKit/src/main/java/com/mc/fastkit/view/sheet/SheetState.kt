package com.mc.fastkit.view.sheet

/**
 *
 * @author: MasterChan
 * @date: 2024-03-04 14:29
 */
sealed class SheetState(val value: Int) {
    /**
     * 拖拽
     */
    data object Dragging : SheetState(0)

    /**
     * 全展开
     */
    data object Expanded : SheetState(1)

    /**
     * 半展开
     */
    data object HalfExpanded : SheetState(2)

    /**
     * 收缩
     */
    data object Collapsed : SheetState(3)

    /**
     * 隐藏
     */
    data object Hidden : SheetState(4)

    /**
     * 状态切换时的中间状态
     */
    data object Settling : SheetState(5)
}