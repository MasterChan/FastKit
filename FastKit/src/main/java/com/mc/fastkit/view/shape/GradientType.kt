package com.mc.fastkit.view.shape

import androidx.annotation.IntDef

@IntDef(GradientType.NONE, GradientType.LINEAR, GradientType.RADIAL, GradientType.SWEEP)
@Retention(AnnotationRetention.SOURCE)
annotation class GradientType {
    companion object {
        const val NONE = -1
        const val LINEAR = 0
        const val RADIAL = 1
        const val SWEEP = 2
    }
}