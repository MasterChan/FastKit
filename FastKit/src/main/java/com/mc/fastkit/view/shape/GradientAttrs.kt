package com.mc.fastkit.view.shape

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import androidx.annotation.ColorInt
import com.mc.fastkit.ext.equalsMutable

/**
 * IGradientBuilder
 * @author: MasterChan
 * @date: 2023-11-07 11:21
 */
interface IGradientBuilder {
    fun setRadius(radius: Float): IGradientBuilder
    fun setTopLeftRadius(radius: Float): IGradientBuilder
    fun setBottomLeftRadius(radius: Float): IGradientBuilder
    fun setTopRightRadius(radius: Float): IGradientBuilder
    fun setBottomRightRadius(radius: Float): IGradientBuilder
    fun setContentColor(@ColorInt color: Int): IGradientBuilder
    fun setContentColor(color: String): IGradientBuilder
    fun setContentMaskColor(@ColorInt color: Int): IGradientBuilder
    fun setContentMaskColor(color: String): IGradientBuilder
    fun setStrokeWidth(width: Int): IGradientBuilder
    fun setStrokeColor(@ColorInt color: Int): IGradientBuilder
    fun setStrokeColor(color: String): IGradientBuilder
    fun setDashWidth(width: Float): IGradientBuilder
    fun setDashGap(gapWidth: Float): IGradientBuilder
    fun setGradientType(@GradientType type: Int): IGradientBuilder
    fun setGradientOrientation(orientation: GradientDrawable.Orientation): IGradientBuilder
    fun setGradientRadius(radius: Float): IGradientBuilder
    fun setGradientCenterX(centerX: Float): IGradientBuilder
    fun setGradientCenterY(centerY: Float): IGradientBuilder
    fun setStartColor(@ColorInt color: Int): IGradientBuilder
    fun setStartColor(color: String): IGradientBuilder
    fun setCenterColor(@ColorInt color: Int): IGradientBuilder
    fun setCenterColor(color: String): IGradientBuilder
    fun setEndColor(@ColorInt color: Int): IGradientBuilder
    fun setEndColor(color: String): IGradientBuilder
}

/**
 * [GradientDrawable]的属性
 * @author: MasterChan
 * @date: 2023-11-07 11:21
 * @property topLeftRadius 左上角半径
 * @property bottomLeftRadius 左下角半径
 * @property topRightRadius 右上角半径
 * @property bottomRightRadius 右下角半径
 * @property contentColor drawable颜色
 * @property strokeWidth 边框宽度
 * @property strokeColor 边框颜色
 * @property dashWidth 虚线宽度
 * @property dashGap 虚线间隔
 * @property gradientType 渐变类型
 * @property gradientOrientation 渐变方向，只有当渐变类型为[GradientType.LINEAR]时有效
 * @property gradientRadius 渐变半径，只有当渐变类型为[GradientType.RADIAL]时有效
 * @property gradientCenterX 渐变中心颜色，当渐变类型为[GradientType.LINEAR]时无效
 * @property gradientCenterY 渐变中心颜色，当渐变类型为[GradientType.LINEAR]时无效
 * @property startColor 渐变开始颜色
 * @property centerColor 渐变中心颜色
 * @property endColor 渐变结束颜色
 */
open class GradientAttrs(private val shaped: IShapedView) : IGradientBuilder {
    var topLeftRadius: Float = 0f
        private set
    var bottomLeftRadius: Float = 0f
        private set
    var topRightRadius: Float = 0f
        private set
    var bottomRightRadius: Float = 0f
        private set
    var contentColor: Int = 0
        private set
    var contentMaskColor: Int = 0
        private set
    var strokeWidth: Int = 0
        private set
    var strokeColor: Int = 0
        private set
    var dashWidth: Float = 0f
        private set
    var dashGap: Float = 0f
        private set
    var gradientType: Int = GradientType.NONE
        private set
    var gradientOrientation: GradientDrawable.Orientation = GradientDrawable.Orientation.TOP_BOTTOM
        private set
    var gradientRadius: Float = 0.5f
        private set
    var gradientCenterX: Float = 0.5f
        private set
    var gradientCenterY: Float = 0f
        private set
    var startColor: Int = 0
        private set
    var centerColor: Int = 0
        private set
    var endColor: Int = 0
        private set

    /**
     * 是否是圆角
     */
    val isRoundedRect: Boolean
        get() {
            return topLeftRadius.equalsMutable(
                bottomLeftRadius, topRightRadius, bottomRightRadius
            ) && topLeftRadius > 0
        }

    override fun setRadius(radius: Float) = apply {
        topLeftRadius = radius
        bottomLeftRadius = radius
        topRightRadius = radius
        bottomRightRadius = radius
    }

    override fun setTopLeftRadius(radius: Float) = apply {
        topLeftRadius = radius
    }

    override fun setBottomLeftRadius(radius: Float) = apply {
        bottomLeftRadius = radius
    }

    override fun setTopRightRadius(radius: Float) = apply {
        topRightRadius = radius
    }

    override fun setBottomRightRadius(radius: Float) = apply {
        bottomRightRadius = radius
    }

    override fun setContentColor(@ColorInt color: Int) = apply {
        contentColor = color
    }

    override fun setContentColor(color: String) = apply {
        contentColor = Color.parseColor(color)
    }

    override fun setContentMaskColor(color: Int) = apply {
        contentMaskColor = color
    }

    override fun setContentMaskColor(color: String) = apply {
        contentMaskColor = Color.parseColor(color)
    }

    override fun setStrokeWidth(width: Int) = apply {
        strokeWidth = width
    }

    override fun setStrokeColor(@ColorInt color: Int) = apply {
        strokeColor = color
    }

    override fun setStrokeColor(color: String) = apply {
        strokeColor = Color.parseColor(color)
    }

    override fun setDashWidth(width: Float) = apply {
        dashWidth = width
    }

    override fun setDashGap(gapWidth: Float) = apply {
        dashGap = gapWidth
    }

    override fun setGradientType(@GradientType type: Int) = apply {
        gradientType = type
    }

    override fun setGradientOrientation(orientation: GradientDrawable.Orientation) = apply {
        gradientOrientation = orientation
    }

    override fun setGradientRadius(radius: Float) = apply {
        gradientRadius = radius
    }

    override fun setGradientCenterX(centerX: Float) = apply {
        gradientCenterX = centerX
    }

    override fun setGradientCenterY(centerY: Float) = apply {
        gradientCenterY = centerY
    }

    override fun setStartColor(@ColorInt color: Int) = apply {
        startColor = color
    }

    override fun setStartColor(color: String) = apply {
        startColor = Color.parseColor(color)
    }

    override fun setCenterColor(@ColorInt color: Int) = apply {
        centerColor = color
    }

    override fun setCenterColor(color: String) = apply {
        centerColor = Color.parseColor(color)
    }

    override fun setEndColor(@ColorInt color: Int) = apply {
        endColor = color
    }

    override fun setEndColor(color: String) = apply {
        endColor = Color.parseColor(color)
    }

    /**
     * 属性设置结束
     * @return IShapedView
     */
    fun without(): IShapedView = shaped

    /**
     * 将属性应用到目标View上
     * @param target View
     */
    fun into(target: View) = shaped.into(target)

    /**
     * 将修改的属性应用到目标View上
     */
    fun setup() = shaped.setup()
}

/**
 * GradientAttrs
 * @author: MasterChan
 * @date: 2023-11-07 11:33
 */
class NormalGradientAttrs(private val shaped: IShapedView) : GradientAttrs(shaped) {

    override fun setRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setRadius(radius)
        shaped.disableAttrs.setRadius(radius)
        return super.setRadius(radius)
    }

    override fun setTopLeftRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setTopLeftRadius(radius)
        shaped.disableAttrs.setTopLeftRadius(radius)
        return super.setTopLeftRadius(radius)
    }

    override fun setBottomLeftRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setBottomLeftRadius(radius)
        shaped.disableAttrs.setBottomLeftRadius(radius)
        return super.setBottomLeftRadius(radius)
    }

    override fun setTopRightRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setTopRightRadius(radius)
        shaped.disableAttrs.setTopRightRadius(radius)
        return super.setTopRightRadius(radius)
    }

    override fun setBottomRightRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setBottomRightRadius(radius)
        shaped.disableAttrs.setBottomRightRadius(radius)
        return super.setBottomRightRadius(radius)
    }

    override fun setContentColor(color: Int): GradientAttrs {
        shaped.stateAttrs.setContentColor(color)
        shaped.disableAttrs.setContentColor(color)
        return super.setContentColor(color)
    }

    override fun setContentColor(color: String): GradientAttrs {
        shaped.stateAttrs.setContentColor(color)
        shaped.disableAttrs.setContentColor(color)
        return super.setContentColor(color)
    }

    override fun setStrokeWidth(width: Int): GradientAttrs {
        shaped.stateAttrs.setStrokeWidth(width)
        shaped.disableAttrs.setStrokeWidth(width)
        return super.setStrokeWidth(width)
    }

    override fun setStrokeColor(color: Int): GradientAttrs {
        shaped.stateAttrs.setStrokeColor(color)
        shaped.disableAttrs.setStrokeColor(color)
        return super.setStrokeColor(color)
    }

    override fun setStrokeColor(color: String): GradientAttrs {
        shaped.stateAttrs.setStrokeColor(color)
        shaped.disableAttrs.setStrokeColor(color)
        return super.setStrokeColor(color)
    }

    override fun setDashWidth(width: Float): GradientAttrs {
        shaped.stateAttrs.setDashWidth(width)
        shaped.disableAttrs.setDashWidth(width)
        return super.setDashWidth(width)
    }

    override fun setDashGap(gapWidth: Float): GradientAttrs {
        shaped.stateAttrs.setDashGap(gapWidth)
        shaped.disableAttrs.setDashGap(gapWidth)
        return super.setDashGap(gapWidth)
    }

    override fun setGradientType(type: Int): GradientAttrs {
        shaped.stateAttrs.setGradientType(type)
        shaped.disableAttrs.setGradientType(type)
        return super.setGradientType(type)
    }

    override fun setGradientOrientation(orientation: GradientDrawable.Orientation): GradientAttrs {
        shaped.stateAttrs.setGradientOrientation(orientation)
        shaped.disableAttrs.setGradientOrientation(orientation)
        return super.setGradientOrientation(orientation)
    }

    override fun setGradientRadius(radius: Float): GradientAttrs {
        shaped.stateAttrs.setGradientRadius(radius)
        shaped.disableAttrs.setGradientRadius(radius)
        return super.setGradientRadius(radius)
    }

    override fun setGradientCenterX(centerX: Float): GradientAttrs {
        shaped.stateAttrs.setGradientCenterX(centerX)
        shaped.disableAttrs.setGradientCenterX(centerX)
        return super.setGradientCenterX(centerX)
    }

    override fun setGradientCenterY(centerY: Float): GradientAttrs {
        shaped.stateAttrs.setGradientCenterY(centerY)
        shaped.disableAttrs.setGradientCenterY(centerY)
        return super.setGradientCenterY(centerY)
    }

    override fun setStartColor(color: Int): GradientAttrs {
        shaped.stateAttrs.setStartColor(color)
        shaped.disableAttrs.setStartColor(color)
        return super.setStartColor(color)
    }

    override fun setStartColor(color: String): GradientAttrs {
        shaped.stateAttrs.setStartColor(color)
        shaped.disableAttrs.setStartColor(color)
        return super.setStartColor(color)
    }

    override fun setCenterColor(color: Int): GradientAttrs {
        shaped.stateAttrs.setCenterColor(color)
        shaped.disableAttrs.setCenterColor(color)
        return super.setCenterColor(color)
    }

    override fun setCenterColor(color: String): GradientAttrs {
        shaped.stateAttrs.setCenterColor(color)
        shaped.disableAttrs.setCenterColor(color)
        return super.setCenterColor(color)
    }

    override fun setEndColor(color: Int): GradientAttrs {
        shaped.stateAttrs.setEndColor(color)
        shaped.disableAttrs.setEndColor(color)
        return super.setEndColor(color)
    }

    override fun setEndColor(color: String): GradientAttrs {
        shaped.stateAttrs.setEndColor(color)
        shaped.disableAttrs.setEndColor(color)
        return super.setEndColor(color)
    }
}