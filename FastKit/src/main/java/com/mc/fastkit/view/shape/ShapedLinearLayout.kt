package com.mc.fastkit.view.shape

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout

/**
 * ShapedLinearLayout
 * @author: MasterChan
 * @date: 2023-03-11 21:36
 */
open class ShapedLinearLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), IShapedView by ShapedViewDelegate(context, attrs) {

    init {
        this.init()
    }

    protected open fun init() {
        into(this)
    }
}