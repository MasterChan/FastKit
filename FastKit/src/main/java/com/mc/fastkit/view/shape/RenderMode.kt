package com.mc.fastkit.view.shape

import androidx.annotation.IntDef

@IntDef(RenderMode.BACKGROUND, RenderMode.FOREGROUND)
@Retention(AnnotationRetention.SOURCE)
annotation class RenderMode {
    companion object {
        const val BACKGROUND = 0
        const val FOREGROUND = 1
    }
}