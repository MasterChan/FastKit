package com.mc.fastkit.view.host

import android.content.Context
import android.util.AttributeSet
import android.view.View
import com.mc.fastkit.ext.dp2pxi
import com.mc.fastkit.ext.statusBarHeight

/**
 * 状态栏占位
 * @author: MasterChan
 * @date: 2024-09-03 9:31
 */
class StatusBarHost @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : View(context, attrs, defStyleAttr, defStyleRes) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (isInEditMode) {
            super.onMeasure(
                widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(dp2pxi(30), MeasureSpec.EXACTLY)
            )
        } else {
            super.onMeasure(
                widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(context.statusBarHeight, MeasureSpec.EXACTLY)
            )
        }
    }
}