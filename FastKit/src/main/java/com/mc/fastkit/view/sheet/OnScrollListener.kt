package com.mc.fastkit.view.sheet

/**
 * OnScrollListener
 * @author: MasterChan
 * @date: 2024-03-05 15:58
 */
fun interface OnScrollListener {
    fun onScroll(dy: Float)
}