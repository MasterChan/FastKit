package com.mc.fastkit.view.shape

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.mc.fastkit.ext.app
import com.mc.fastkit.ext.getDrawableStrict

/**
 * ShapedView接口定义
 * @author: MasterChan
 * @date: 2022-06-29 22:30
 */
interface IShapedView {

    /**
     * 是否为圆形
     */
    var isRounded: Boolean

    /**
     * 是否使用水波纹效果
     */
    var useRipple: Boolean

    /**
     * 水波纹颜色
     */
    var rippleColor: Int

    /**
     * 水波纹的Mask
     */
    var rippleMask: Drawable?

    /**
     * 设置后[normalAttrs]不再生效
     */
    var contentDrawable: Drawable?

    /**
     * 设置后[stateAttrs]不再生效
     */
    var contentDrawableState: Drawable?

    /**
     * 设置后[disableAttrs]不再生效
     */
    var contentDrawableDisable: Drawable?

    /**
     * normal状态下的属性
     */
    var normalAttrs: GradientAttrs

    /**
     * state状态下的属性
     */
    var stateAttrs: GradientAttrs

    /**
     * 当[View.setEnabled]为false时的属性
     */
    var disableAttrs: GradientAttrs

    /**
     * 设置state的类型，默认为[StateType.NONE]
     */
    var stateType: Int

    /**
     * 渲染模式 background or foreground
     */
    var renderMode: Int

    fun setRounded(isRounded: Boolean) = apply {
        this.isRounded = isRounded
    }

    fun setUseRipple(useRipple: Boolean) = apply {
        this.useRipple = useRipple
    }

    fun setRippleColor(@ColorInt color: Int) = apply {
        this.rippleColor = color
    }

    fun setRippleColor(color: String) = apply {
        setRippleColor(Color.parseColor(color))
    }

    fun setRippleMask(drawable: Int) = apply {
        setRippleMask(app.getDrawableStrict(drawable))
    }

    fun setRippleMask(drawable: Drawable?) = apply {
        this.rippleMask = drawable
    }

    fun setContentDrawable(@DrawableRes drawable: Int) = apply {
        setContentDrawable(app.getDrawableStrict(drawable))
    }

    fun setContentDrawableState(@DrawableRes drawable: Int) = apply {
        setContentDrawableState(app.getDrawableStrict(drawable))
    }

    fun setContentDrawableDisable(@DrawableRes drawable: Int) = apply {
        setContentDrawableDisable(app.getDrawableStrict(drawable))
    }

    fun setContentDrawable(drawable: Drawable?) = apply {
        this.contentDrawable = drawable
    }

    fun setContentDrawableState(drawable: Drawable?) = apply {
        this.contentDrawableState = drawable
    }

    fun setContentDrawableDisable(drawable: Drawable?) = apply {
        this.contentDrawableDisable = drawable
    }

    fun setStateType(stateType: Int) = apply {
        this.stateType = stateType
    }

    fun setRenderMode(@RenderMode renderMode: Int) = apply {
        this.renderMode = renderMode
    }

    fun withNormal(): GradientAttrs = normalAttrs

    fun withState(): GradientAttrs = stateAttrs

    fun withDisable(): GradientAttrs = disableAttrs

    fun into(target: View)

    fun setup()

    fun getStateByType(): Int {
        return when (stateType) {
            StateType.PRESSED -> android.R.attr.state_pressed
            StateType.SELECTED -> android.R.attr.state_selected
            StateType.CHECKED -> android.R.attr.state_checked
            else -> StateType.NONE
        }
    }
}