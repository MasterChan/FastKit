package com.mc.fastkit.view.shape

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatTextView
import com.mc.fastkit.R

/**
 * ShapedTextView
 * @author: MasterChan
 * @date: 2023-03-11 21:36
 */
open class ShapedTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr),
    IShapedView by ShapedViewDelegate(context, attrs) {

    private var txtColorState = 0
    private var textColorDisable = 0

    init {
        this.initAttrs(attrs)
        this.init()
    }

    protected open fun initAttrs(attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.ShapedTextView)
        txtColorState = a.getColor(R.styleable.ShapedTextView_mc_textColorState, getTextColor())
        textColorDisable = a.getColor(
            R.styleable.ShapedTextView_mc_textColorDisable, getTextColor()
        )
        setTextColor(getTextColor(), txtColorState, textColorDisable)
        a.recycle()
    }

    protected open fun init() {
        into(this)
    }

    fun setTextColorState(@ColorInt textColorState: Int) = apply {
        setTextColor(getTextColor(), textColorState, textColorDisable)
    }

    fun setTextColorDisable(@ColorInt textColorDisable: Int) = apply {
        setTextColor(getTextColor(), txtColorState, textColorDisable)
    }

    fun setTextColor(
        @ColorInt textColor: Int,
        @ColorInt textColorState: Int,
        @ColorInt textColorDisable: Int
    ) = apply {
        //textColorDisable不属于state状态
        if (stateType == StateType.NONE || textColorState == 0) {
            val states = arrayOf(
                intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_enabled)
            )
            setTextColor(
                ColorStateList(states, intArrayOf(textColor, textColorDisable))
            )
        } else {
            val states = arrayOf(
                intArrayOf(getStateByType()), intArrayOf(android.R.attr.state_enabled),
                intArrayOf(-android.R.attr.state_enabled)
            )
            setTextColor(
                ColorStateList(states, intArrayOf(textColorState, textColor, textColorDisable))
            )
        }
    }

    private fun getTextColor(): Int {
        return if (textColors.isStateful) {
            textColors.getColorForState(intArrayOf(android.R.attr.state_enabled), Color.BLACK)
        } else {
            textColors.defaultColor
        }
    }

    override fun setStateType(@StateType stateType: Int): IShapedView {
        super.setStateType(stateType)
        setTextColor(getTextColor(), txtColorState, textColorDisable)
        return this
    }
}