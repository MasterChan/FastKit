package com.mc.fastkit.view

import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.view.MotionEvent
import com.mc.fastkit.R
import com.mc.fastkit.view.shape.ShapedImageView

/**
 * FilterImageView
 * @author: MasterChan
 * @date: 2024-08-30 11:46
 */
class FilterImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ShapedImageView(context, attrs, defStyleAttr) {

    var filterColor = 0

    init {
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.FilterImageView, defStyleAttr, 0
        )
        filterColor = a.getColor(R.styleable.FilterImageView_mc_filterColor, 0)
        a.recycle()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (filterColor != 0) {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> setColorFilter(filterColor, PorterDuff.Mode.DST_OVER)

                MotionEvent.ACTION_UP -> clearColorFilter()
            }
        }
        return super.onTouchEvent(event)
    }
}