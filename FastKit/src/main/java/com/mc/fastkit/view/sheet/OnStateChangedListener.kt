package com.mc.fastkit.view.sheet

/**
 * OnStateChangedListener
 * @author: MasterChan
 * @date: 2024-03-05 15:47
 */
fun interface OnStateChangedListener {
    fun onStateChanged(oldState: SheetState, newState: SheetState)
}