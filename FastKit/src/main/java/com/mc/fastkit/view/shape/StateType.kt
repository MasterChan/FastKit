package com.mc.fastkit.view.shape

import androidx.annotation.IntDef

@IntDef(StateType.NONE, StateType.PRESSED, StateType.SELECTED, StateType.CHECKED)
@Retention(AnnotationRetention.SOURCE)
annotation class StateType {
    companion object {
        const val NONE = -1
        const val PRESSED = 0
        const val SELECTED = 1
        const val CHECKED = 2
    }
}