package com.mc.fastkit.ext

import android.util.Base64
import com.mc.fastkit.utils.DecodeUtils
import com.mc.fastkit.utils.EncodeUtils
import java.util.*

fun ByteArray.encode2Base64(flags: Int = Base64.NO_WRAP): ByteArray {
    return EncodeUtils.encode2Base64(this, flags)
}

fun ByteArray.encode2Base64String(flags: Int = Base64.NO_WRAP): String {
    return EncodeUtils.encode2Base64String(this, flags)
}

fun ByteArray.decodeBase64(): ByteArray {
    return DecodeUtils.decodeBase64(this)
}

fun ByteArray.toHexString(): String {
    val ret = CharArray(size shl 1)
    var i = 0
    var j = 0
    val hexDigits = charArrayOf(
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    )
    while (i < size) {
        ret[j++] = hexDigits[get(i).toInt() ushr 4 and 0x0f]
        ret[j++] = hexDigits[get(i).toInt() and 0x0f]
        i++
    }
    return String(ret)
}