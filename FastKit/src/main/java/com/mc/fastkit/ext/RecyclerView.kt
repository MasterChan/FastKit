package com.mc.fastkit.ext

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import androidx.recyclerview.widget.StaggeredGridLayoutManager

val RecyclerView.linearLayoutManager: LinearLayoutManager?
    get() = layoutManager?.cast()
val RecyclerView.gridLayoutManager: GridLayoutManager?
    get() = layoutManager?.cast()
val RecyclerView.staggeredGridLayoutManager: StaggeredGridLayoutManager?
    get() = layoutManager?.cast()

fun RecyclerView.disableAnimate() {
    (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
}

fun RecyclerView.scrollToPositionWithOffset(position: Int, offset: Int = 0) {
    val manager = layoutManager ?: return
    when (manager) {
        is GridLayoutManager -> manager.scrollToPositionWithOffset(position, offset)
        is LinearLayoutManager -> manager.scrollToPositionWithOffset(position, offset)
        is StaggeredGridLayoutManager -> manager.scrollToPositionWithOffset(position, offset)
    }
}