package com.mc.fastkit.ext

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.recyclerview.widget.RecyclerView
import com.mc.fastkit.widget.XDivider

fun RecyclerView.divider(spacing: Float, @ColorInt color: Int) {
    XDivider().setSpacing(spacing).setDividerColor(color).into(this)
}

fun RecyclerView.divider(spacing: Float, color: String) {
    divider(spacing, Color.parseColor(color))
}

fun RecyclerView.dividerBoth(spacing: Float, @ColorInt color: Int) {
    XDivider().setSpacing(spacing)
        .setOrientation(RecyclerView.HORIZONTAL)
        .setDividerColor(color)
        .into(this)
    XDivider().setSpacing(spacing)
        .setOrientation(RecyclerView.VERTICAL)
        .setDividerColor(color)
        .into(this)
}

fun RecyclerView.dividerBoth(spacing: Float, color: String) {
    dividerBoth(spacing, color)
}