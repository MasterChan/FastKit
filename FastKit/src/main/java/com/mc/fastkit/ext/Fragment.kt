package com.mc.fastkit.ext

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import com.mc.fastkit.utils.ActivityUtils

fun Fragment.startActivity(clazz: Class<out Activity>) {
    ActivityUtils.startActivity(requireContext(), clazz)
}

inline fun <reified T : Activity> Fragment.startActivity(noinline params: (Intent.() -> Unit)? = null) {
    ActivityUtils.startActivity<T>(requireContext(), params)
}

inline fun <reified T : Activity> Fragment.startActivity(
    enterAnimate: Int,
    exitAnimate: Int,
    noinline params: (Intent.() -> Unit)? = null
) {
    ActivityUtils.startActivity<T>(requireContext(), enterAnimate, exitAnimate, params)
}

inline fun <reified T : Activity> Fragment.startActivity(
    vararg sharedElements: Pair<View, String>,
    noinline params: (Intent.() -> Unit)? = null
) {
    ActivityUtils.startActivity<T>(requireContext(), *sharedElements) { params?.invoke(this) }
}