package com.mc.fastkit.ext

import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.text.method.DigitsKeyListener
import android.view.Gravity
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.GravityInt
import androidx.annotation.StringRes

fun TextView.setText(@StringRes res: Int, vararg format: Any) {
    text = context.getString(res, format)
}

fun TextView.setDigits(digits: String) {
    keyListener = DigitsKeyListener.getInstance(digits)
}

fun TextView.addUnderLine() {
    paint.flags = paint.flags or Paint.UNDERLINE_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.addDeleteLine() {
    paint.flags = paint.flags or Paint.STRIKE_THRU_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.removeUnderLine() {
    paintFlags = paintFlags and Paint.UNDERLINE_TEXT_FLAG.inv()
}

fun TextView.removeDeleteLine() {
    paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
}

fun TextView.bold() {
    paint.isFakeBoldText = true
    paint.isAntiAlias = true
}

fun TextView.font(fontName: String, fontFolder: String = "fonts") {
    typeface = Typeface.createFromAsset(context.assets, "$fontFolder/$fontName")
}

fun TextView.setDrawable(@GravityInt gravity: Int, @DrawableRes res: Int) {
    setDrawable(gravity, getDrawableStrict(res))
}

fun TextView.setDrawable(@GravityInt gravity: Int, drawable: Drawable?) {
    when (gravity) {
        Gravity.START -> setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
        Gravity.TOP -> setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
        Gravity.END -> setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
        Gravity.BOTTOM -> setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
    }
}

fun TextView.setDrawable(
    @GravityInt gravity: Int,
    drawable: Drawable?,
    width: Int,
    height: Int,
    padding: Int
) {
    val paddingDrawable = LayerDrawable(arrayOf(drawable))
    when (gravity) {
        Gravity.START -> setDrawable(gravity, paddingDrawable, width, height, padding, 0, 0, 0)
        Gravity.TOP -> setDrawable(gravity, paddingDrawable, width, height, 0, padding, 0, 0)
        Gravity.END -> setDrawable(gravity, paddingDrawable, width, height, 0, 0, padding, 0)
        Gravity.BOTTOM -> setDrawable(gravity, paddingDrawable, width, height, 0, 0, 0, padding)
    }
}

fun TextView.setDrawable(
    @GravityInt gravity: Int,
    drawable: Drawable?,
    width: Int,
    height: Int,
    paddingStart: Int,
    paddingTop: Int,
    paddingEnd: Int,
    paddingBottom: Int
) {
    val paddingDrawable = LayerDrawable(arrayOf(drawable))
    paddingDrawable.setLayerInset(0, paddingStart, paddingTop, paddingEnd, paddingBottom)
    paddingDrawable.setBounds(
        0, 0, width + paddingStart + paddingEnd, height + paddingTop + paddingBottom
    )
    when (gravity) {
        Gravity.START -> setCompoundDrawables(paddingDrawable, null, null, null)
        Gravity.TOP -> setCompoundDrawables(null, paddingDrawable, null, null)
        Gravity.END -> setCompoundDrawables(null, null, paddingDrawable, null)
        Gravity.BOTTOM -> setCompoundDrawables(null, null, null, paddingDrawable)
    }
}