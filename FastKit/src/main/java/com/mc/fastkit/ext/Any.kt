package com.mc.fastkit.ext

import android.os.Looper

val isMainThread: Boolean
    get() = Looper.myLooper() == Looper.getMainLooper()

/**
 * 类型转换
 * @receiver Any
 * @return T
 */
@Suppress("UNCHECKED_CAST")
fun <T> Any.cast(): T = this as T

/**
 * 类型转换，与[cast]不同的是，如果转换的类型为null，则返回null
 * @receiver Any?
 * @return T?
 */
inline fun <reified T> Any?.castOrNull(): T? {
    return if (this is T) this else null
}

/**
 * 如果类型不为null，返回给定的值，否则返回null
 * @receiver T?
 * @param predicate Function1<T, M>
 * @return M?
 */
fun <T, M> T?.takeNotNull(predicate: (T) -> M): M? {
    return if (this != null) predicate(this) else null
}

/**
 * 同时与多个参数比较是否相等
 * @receiver T
 * @param args Array<out T>
 * @return Boolean
 */
fun <T> T.equalsMutable(vararg args: T): Boolean {
    args.forEach { if (it != this) return false }
    return true
}
