package com.mc.fastkit.ext

import android.content.res.TypedArray
import android.graphics.Color
import androidx.annotation.StyleableRes

fun TypedArray.getColor(@StyleableRes index: Int, defValue: String): Int {
    return getColor(index, Color.parseColor(defValue))
}

fun TypedArray.getString(@StyleableRes index: Int, defValue: String): String {
    return getString(index) ?: defValue
}