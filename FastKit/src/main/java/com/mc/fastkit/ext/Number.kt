package com.mc.fastkit.ext

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

/**
 * 数字格式化，4舍5入
 * @param decimals 保留的小数位数
 * @return String
 */
fun Number.decimalFixed(decimals: Int = 1): String {
    return "%.${decimals}f".format(this)
}

/**
 * 数字格式化
 * @param decimals 保留的小数位数
 * @param roundingMode 格式化的模式，默认为四舍五入
 * @return String
 */
fun Number.decimalRetain(decimals: Int = 1, roundingMode: RoundingMode): String {
    //格式化时不使用本地化，避免出现在不同国家千位分隔符和小数点分隔符不一致的情况
    val format = if (decimals >= 1) {
        DecimalFormat("#." + "0".repeat(decimals), DecimalFormatSymbols.getInstance(Locale.CHINA))
    } else {
        DecimalFormat("#", DecimalFormatSymbols.getInstance(Locale.CHINA))
    }
    format.isGroupingUsed = false
    //禁止对格式化的结果分组
    format.roundingMode = roundingMode
    return format.format(this)
}

/**
 * 数字格式化
 * @param decimals 保留的小数位数
 * @param isRounds 是否4舍5入
 * @return String
 */
fun Number.decimalRetain(decimals: Int = 1, isRounds: Boolean = true): String {
    return decimalRetain(decimals, if (isRounds) RoundingMode.HALF_UP else RoundingMode.DOWN)
}

/**
 * 去除小数末尾的0
 * @receiver Double
 * @return String
 */
fun Number.stripZeros(): String {
    return BigDecimal(toString()).stripTrailingZeros().toPlainString()
}