package com.mc.fastkit.ext

import com.mc.fastkit.FastKit
import com.mc.fastkit.widget.ActivityStack

val app = FastKit.app

val isAppForeground: Boolean
    get() = ActivityStack.instance.isAppForeground