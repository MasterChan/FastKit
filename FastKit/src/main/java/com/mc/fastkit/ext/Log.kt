package com.mc.fastkit.ext

import com.mc.fastkit.log.LogUtils
import com.mc.fastkit.log.Priority
import com.mc.fastkit.utils.StackTraceUtils

typealias Log = LogUtils

fun Any?.logV() = println(Priority.VERBOSE)

fun Any?.logD() = println(Priority.DEBUG)

fun Any?.logI() = println(Priority.INFO)

fun Any?.logW() = println(Priority.WARN)

fun Any?.logE() = println(Priority.ERROR)

fun Any?.logA() = println(Priority.ASSERT)

fun Any?.logWtf() = println(Priority.WARN)

fun Any?.println(@Priority priority: Int = Priority.DEBUG) {
    LogUtils.print(
        priority, LogUtils.tag,
        StackTraceUtils.getTargetStackTraceElement("com.mc.fastkit.ext.LogKt"), this
    )
}