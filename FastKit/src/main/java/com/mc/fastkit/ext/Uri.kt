package com.mc.fastkit.ext

import android.net.Uri
import com.mc.fastkit.utils.UriUtils

val Uri.realPath: String?
    get() = UriUtils.getPath(app, this)