package com.mc.fastkit.ext

import android.widget.Toast
import androidx.annotation.StringRes
import com.mc.fastkit.utils.StackTraceUtils
import com.mc.fastkit.utils.ToastUtils

fun toast(@StringRes text: Int, newConfig: ToastUtils.Config? = null) {
    toast(app.getString(text), newConfig)
}

fun toast(text: CharSequence, newConfig: ToastUtils.Config? = null) {
    ToastUtils.show(text, Toast.LENGTH_SHORT, newConfig, getStackTraceElement())
}

fun toastLong(@StringRes text: Int, newConfig: ToastUtils.Config? = null) {
    toastLong(app.getString(text), newConfig)
}

fun toastLong(text: CharSequence, newConfig: ToastUtils.Config? = null) {
    ToastUtils.show(text, Toast.LENGTH_LONG, newConfig, getStackTraceElement())
}

private fun getStackTraceElement(): StackTraceElement? {
    return StackTraceUtils.getTargetStackTraceElement("com.mc.fastkit.ext.ToastKt")
}