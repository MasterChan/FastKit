package com.mc.fastkit.ext

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment

fun Context.color(@ColorRes resId: Int): Int = getColor(resId)
fun Context.color(colorStr: String): Int = Color.parseColor(colorStr)

fun View.color(@ColorRes resId: Int): Int = context.color(resId)
fun View.color(colorStr: String): Int = context.color(colorStr)

fun Fragment.color(@ColorRes resId: Int): Int = requireContext().color(resId)
fun Fragment.color(colorStr: String): Int = requireContext().color(colorStr)

fun Context.colorStateList(@ColorRes resId: Int) = ColorStateList.valueOf(getColor(resId))
fun Context.colorStateList(colorStr: String) = ColorStateList.valueOf(Color.parseColor(colorStr))

fun View.colorStateList(@ColorRes resId: Int) = ColorStateList.valueOf(context.color(resId))
fun View.colorStateList(colorStr: String) = ColorStateList.valueOf(context.color(colorStr))

fun Fragment.colorStateList(@ColorRes resId: Int) =
    ColorStateList.valueOf(requireContext().color(resId))

fun Fragment.colorStateList(colorStr: String) =
    ColorStateList.valueOf(requireContext().color(colorStr))