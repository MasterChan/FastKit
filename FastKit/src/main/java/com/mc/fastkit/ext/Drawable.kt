package com.mc.fastkit.ext

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.core.graphics.drawable.toDrawable
import androidx.fragment.app.Fragment

fun Context.getDrawableStrict(@DrawableRes id: Int): Drawable? {
    return ContextCompat.getDrawable(this, id)
}

fun Fragment.getDrawableStrict(@DrawableRes id: Int): Drawable? {
    return ContextCompat.getDrawable(requireContext(), id)
}

fun View.getDrawableStrict(@DrawableRes id: Int): Drawable? {
    return context.getDrawableStrict(id)
}

fun Drawable.toByteArray(
    format: Bitmap.CompressFormat = Bitmap.CompressFormat.PNG,
    quality: Int = 100
): ByteArray {
    return toBitmap().toByteArray(format, quality)
}

fun ByteArray.toDrawable(options: BitmapFactory.Options? = null): Drawable {
    return toBitmap(options).toDrawable(app.resources)
}