package com.mc.fastkit.ext

import com.mc.fastkit.utils.FileUtils
import java.io.File
import java.io.FileFilter

val File.mimeType: String?
    get() = FileUtils.getMimeType(this)

val File.size: Long
    get() = FileUtils.getFileSize(this)

val File.stringSize: String
    get() = FileUtils.getFileSizeString(this)

fun File.createDir() = FileUtils.createDir(this)

fun File.createFile() = FileUtils.createFile(this)

fun File.createFileAndDelOldFile() = FileUtils.createFileAndDelOldFile(this)

fun File.deleteAll() = FileUtils.delete(this)

fun File.renameTo(destName: String?) = FileUtils.rename(this, destName)

fun File.copyTo(destFile: File?) = FileUtils.copy(this, destFile)

fun File.moveTo(destFile: File?) = FileUtils.move(this, destFile)

fun File.listAll(fileFilter: FileFilter? = null) = FileUtils.listFiles(this, fileFilter)