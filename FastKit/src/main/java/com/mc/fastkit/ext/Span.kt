package com.mc.fastkit.ext

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.FontMetricsInt
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.BackgroundColorSpan
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.ImageSpan
import android.text.style.MetricAffectingSpan
import android.text.style.StrikethroughSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.View
import androidx.annotation.DrawableRes

/**
 * 修改指定[range]文字的文字大小
 * @param range 文字范围
 * @param textSize 具体需要修改的值
 */
fun CharSequence.toSizeSpan(range: IntRange, textSize: Float): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            ExactlySizePan(textSize), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

fun CharSequence.toSizeSpan(textSize: Float): CharSequence {
    return toSizeSpan(0..length, textSize)
}

/**
 * 修改指定[range]文字的文字前景色
 * @param range 文字范围
 * @param color 要改变的颜色
 */
fun CharSequence.toColorSpan(range: IntRange, color: Int): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            ForegroundColorSpan(color), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

fun CharSequence.toColorSpan(color: Int): CharSequence {
    return toColorSpan(0..length, color)
}

fun CharSequence.toColorSpan(color: String): CharSequence {
    return toColorSpan(0..length, Color.parseColor(color))
}

/**
 * 修改指定[range]文字的文字背景色
 * @param range 文字范围
 * @param color 要改变的颜色
 */
fun CharSequence.toBackgroundColorSpan(range: IntRange, color: Int): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            BackgroundColorSpan(color), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

fun CharSequence.toBackgroundColorSpan(color: Int): CharSequence {
    return toBackgroundColorSpan(0..length, color)
}

/**
 * 为指定[range]的文字添加删除线
 * @param range 文字范围
 */
fun CharSequence.toStrikethroughSpan(range: IntRange): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            StrikethroughSpan(), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

fun CharSequence.toStrikethroughSpan(): CharSequence {
    return toStrikethroughSpan(0..length)
}

/**
 * 为指定[range]的文字添加下划线
 * @receiver CharSequence
 * @param range IntRange
 * @return CharSequence
 */
fun CharSequence.toUnderlineSpan(range: IntRange): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            UnderlineSpan(), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

fun CharSequence.toUnderlineSpan(): CharSequence {
    return toUnderlineSpan(0..length)
}

/**
 * 为指定[range]的文字添加点击事件
 * @param range 文字范围
 * @param textColor 文字颜色
 * @param clickListener 点击事件
 * @return CharSequence
 */
fun CharSequence.toClickSpan(
    range: IntRange,
    textColor: Int = 0,
    clickListener: ((View) -> Unit)?
): CharSequence {
    return SpannableString(this).apply {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                clickListener?.invoke(widget)
            }

            override fun updateDrawState(textPaint: TextPaint) {
                if (textColor != 0) {
                    textPaint.color = textColor
                }
            }
        }
        setSpan(clickableSpan, range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
}

fun CharSequence.toClickSpan(textColor: Int = 0, clickListener: ((View) -> Unit)?): CharSequence {
    return toClickSpan(0..length, textColor, clickListener)
}

/**
 * 为指定[range]的文字添加Style效果
 * @param range 文字范围
 * @param style 例如[Typeface.BOLD]
 */
fun CharSequence.toStyleSpan(range: IntRange, style: Int): CharSequence {
    return SpannableString(this).apply {
        setSpan(StyleSpan(style), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
}

fun CharSequence.toStyleSpan(style: Int): CharSequence {
    return toStyleSpan(0..length, style)
}

/**
 * 为指定[range]的文字设置字体
 * @param range 文字范围
 * @param typeface 自定义字体
 */
fun CharSequence.toTypeFaceSpan(range: IntRange, typeface: Typeface): CharSequence {
    return SpannableString(this).apply {
        setSpan(TypefaceSpan(typeface), range.first, range.last, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
}

fun CharSequence.toTypeFaceSpan(typeface: Typeface): CharSequence {
    return toTypeFaceSpan(0..length, typeface)
}

/**
 * 为指定[range]的文字设置图片
 * @param range 文字范围
 * @param imageRes 图片
 * @param verticalAlignment 垂直对齐方式
 * @param leftMargin Int
 * @param rightMargin Int
 * @param imageWidth Int
 * @param imageHeight Int
 * @return CharSequence
 */
fun CharSequence.toImageSpan(
    range: IntRange,
    @DrawableRes imageRes: Int,
    verticalAlignment: Int = ExtendImageSpan.ALIGN_CENTER,
    leftMargin: Int = 0,
    rightMargin: Int = 0,
    imageWidth: Int = -1,
    imageHeight: Int = -1
): CharSequence {
    return toImageSpan(
        range, app.getDrawableStrict(imageRes)!!, verticalAlignment, leftMargin, rightMargin,
        imageWidth,
        imageHeight
    )
}

/**
 * 为指定[range]的文字设置图片
 * @param range 文字范围
 * @param drawable 图片
 * @param verticalAlignment 垂直对齐方式
 * @param leftMargin Int
 * @param rightMargin Int
 * @param imageWidth Int
 * @param imageHeight Int
 * @return CharSequence
 */
fun CharSequence.toImageSpan(
    range: IntRange,
    drawable: Drawable,
    verticalAlignment: Int = ExtendImageSpan.ALIGN_CENTER,
    leftMargin: Int = 0,
    rightMargin: Int = 0,
    imageWidth: Int = -1,
    imageHeight: Int = -1
): CharSequence {
    return SpannableString(this).apply {
        setSpan(
            ExtendImageSpan(
                drawable.apply {
                    val width = if (imageWidth == -1) intrinsicWidth else imageWidth
                    val height = if (imageHeight == -1) intrinsicHeight else imageHeight
                    setBounds(0, 0, width, height)
                },
                verticalAlignment,
                leftMargin,
                rightMargin
            ),
            range.first,
            range.last,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}

/**
 * 设置文本的大小，与[android.text.style.RelativeSizeSpan]不同的是，需要一个精确的数值
 * @author: MasterChan
 * @date: 2022-8-18 17:15
 */
open class ExactlySizePan(private val size: Float) : MetricAffectingSpan() {

    override fun updateDrawState(textPaint: TextPaint) {
        textPaint.textSize = sp2px(size)
    }

    override fun updateMeasureState(textPaint: TextPaint) {
        textPaint.textSize = sp2px(size)
    }
}

/**
 * 对[ImageSpan]进行扩展，支持垂直对齐和左右间距
 * @author: MasterChan
 * @date: 2022-8-18 16:18
 */
open class ExtendImageSpan @JvmOverloads constructor(
    drawable: Drawable,
    verticalAlignment: Int = 2,
    private val leftMargin: Int = 0,
    private val rightMargin: Int = 0
) : ImageSpan(drawable, verticalAlignment) {

    companion object Align {
        const val ALIGN_CENTER = 3
    }

    constructor(@DrawableRes res: Int, verticalAlignment: Int) : this(
        app.getDrawableStrict(res)!!, verticalAlignment
    )

    override fun getSize(
        paint: Paint, text: CharSequence, start: Int, end: Int,
        fontMetricsInt: FontMetricsInt?
    ): Int {
        val drawable = drawable
        val rect = drawable.bounds
        if (null != fontMetricsInt) {
            val fmPaint = paint.fontMetricsInt
            val fontHeight = fmPaint.descent - fmPaint.ascent
            val drHeight = rect.bottom - rect.top
            val centerY = fmPaint.ascent + fontHeight / 2
            fontMetricsInt.ascent = centerY - drHeight / 2
            fontMetricsInt.top = fontMetricsInt.ascent
            fontMetricsInt.bottom = centerY + drHeight / 2
            fontMetricsInt.descent = fontMetricsInt.bottom
        }
        return leftMargin + rect.right + rightMargin
    }

    override fun draw(
        canvas: Canvas, text: CharSequence, start: Int, end: Int,
        x: Float, top: Int, y: Int, bottom: Int, paint: Paint
    ) {
        val transX = x + leftMargin
        if (verticalAlignment == ALIGN_CENTER) {
            val drawable = drawable
            canvas.save()
            val fmPaint = paint.fontMetricsInt
            val fontHeight = fmPaint.descent - fmPaint.ascent
            val centerY = y + fmPaint.descent - fontHeight / 2
            val transY = centerY - (drawable.bounds.bottom - drawable.bounds.top) / 2
            canvas.translate(transX, transY.toFloat())
            drawable.draw(canvas)
            canvas.restore()
        } else {
            super.draw(canvas, text, start, end, transX, top, y, bottom, paint)
        }
    }
}

/**
 * 与[android.text.style.StyleSpan]功能相同，不同的是支持自定义字体
 * @author: MasterChan
 * @date: 2022-8-18 18:46
 */
open class TypefaceSpan(private val typeface: Typeface) : MetricAffectingSpan() {

    override fun updateDrawState(drawState: TextPaint) {
        apply(drawState)
    }

    override fun updateMeasureState(paint: TextPaint) {
        apply(paint)
    }

    private fun apply(paint: Paint) {
        val oldTypeface = paint.typeface
        val oldStyle = oldTypeface?.style ?: 0
        val fakeStyle = oldStyle and typeface.style.inv()
        if (fakeStyle and Typeface.BOLD != 0) {
            paint.isFakeBoldText = true
        }
        if (fakeStyle and Typeface.ITALIC != 0) {
            paint.textSkewX = -0.25f
        }
        paint.typeface = typeface
    }
}
