package com.mc.fastkit.ext

import android.view.View
import androidx.viewbinding.ViewBinding

fun ViewBinding.bindClicks(listener: View.OnClickListener?, vararg views: View) {
    views.forEach { it.setOnClickListener(listener) }
}

fun ViewBinding.bindClicks(listener: View.OnClickListener?, views: ViewBinding.() -> Array<View>) {
    views(this).forEach { it.setOnClickListener(listener) }
}

fun ViewBinding.bindSingleClicks(listener: View.OnClickListener?, vararg views: View) {
    bindSingleClicks(500L, listener, views)
}

fun ViewBinding.bindSingleClicks(
    delay: Long,
    listener: View.OnClickListener?,
    views: Array<out View>
) {
    views.forEach { it.setOnSingleClickListener(delay) { view -> listener?.onClick(view) } }
}