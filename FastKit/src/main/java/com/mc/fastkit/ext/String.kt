package com.mc.fastkit.ext

import android.util.Base64
import com.mc.fastkit.utils.EncodeUtils
import java.util.Locale

fun String.toBase64(flags: Int = Base64.NO_WRAP): ByteArray {
    return EncodeUtils.encode2Base64(this.toByteArray(), flags)
}

fun String.toBase64String(flags: Int = Base64.NO_WRAP): String {
    return EncodeUtils.encode2Base64String(this.toByteArray(), flags)
}

fun String.toHexByteArray(): ByteArray {
    require(length % 2 == 0) { "长度不是偶数" }
    val hexBytes = uppercase(Locale.getDefault()).toCharArray()
    val ret = ByteArray(length ushr 1)
    var i = 0
    while (i < length) {
        ret[i shr 1] = (hexBytes[i].hex2Dec() shl 4 or hexBytes[i + 1].hex2Dec()).toByte()
        i += 2
    }
    return ret
}

fun String.hexString2Bytes(): ByteArray {
    var hexString = this
    var len = hexString.length
    if (len % 2 != 0) {
        hexString = "0$hexString"
        len += 1
    }
    val hexBytes = hexString.uppercase(Locale.getDefault()).toCharArray()
    val result = ByteArray(len shr 1)
    var i = 0
    while (i < len) {
        result[i shr 1] = (hexBytes[i].hex2Dec() shl 4 or hexBytes[i + 1].hex2Dec()).toByte()
        i += 2
    }
    return result
}

private fun Char.hex2Dec(): Int {
    return when (this) {
        in '0'..'9' -> {
            this - '0'
        }

        in 'A'..'F' -> {
            this - 'A' + 10
        }

        else -> {
            throw IllegalArgumentException()
        }
    }
}

/**
 * 判断是否是手机号码
 * 中国移动: 134(0-8), 135, 136, 137, 138, 139, 147, 150, 151, 152, 157, 158, 159, 165, 172, 178,
 * 182, 183, 184, 187, 188, 195, 197, 198
 * 中国联通: 130, 131, 132, 145, 155, 156, 166, 167, 175, 176, 185, 186, 196
 * 中国电信: 133, 149, 153, 162, 173, 177, 180, 181, 189, 190, 191, 199
 * 中国广电: 192
 * 移动卫星: 1349
 * 虚拟号: 170, 171
 * @receiver String
 * @return Boolean
 */
fun String.isMobileNum(): Boolean {
    return "^((13[0-9])|(14[579])|(15[0-35-9])|(16[2567])|(17[0-35-8])|(18[0-9])|(19[0-35-9]))\\d{8}\$".toRegex()
        .matches(this)
}

/**
 * 是否是电话号码
 * @receiver String
 * @return Boolean
 */
fun String.isPhoneNum(): Boolean {
    return "^0\\d{2,3}[- ]?\\d{7,8}\$".toRegex().matches(this)
}

/**
 * 是否是身份证号码
 * @receiver String
 * @return Boolean
 */
fun String.isIdCardNum(): Boolean {
    return "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}\$".toRegex().matches(this)
}

/**
 * 判断是否是邮箱
 * @receiver String
 * @return Boolean
 */
fun String.isEmail(): Boolean {
    return "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*\$".toRegex().matches(this)
}

/**
 * 判断是否是域名
 * @receiver String
 * @return Boolean
 */
fun String.isDomain(): Boolean {
    return "[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\\.?".toRegex()
        .matches(this)
}

/**
 * 判断是否是URL地址
 * @receiver String
 * @return Boolean
 */
fun String.isInternetUrl(): Boolean {
    return "[a-zA-z]+://[^\\s]*".toRegex().matches(this)
}

/**
 * 是否是个汉字
 * @receiver String
 * @return Boolean
 */
fun String.isZh(): Boolean {
    return "^[\\u4e00-\\u9fa5]+$".toRegex().matches(this)
}

/**
 * 是否是IP地址
 * @receiver String
 * @return Boolean
 */
fun String.isIpAddress(): Boolean {
    return "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)".toRegex()
        .matches(this)
}
