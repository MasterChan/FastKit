package com.mc.fastkit.ext

import com.mc.fastkit.utils.DateUtils
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

val Date.year2: Int
    get() = DateUtils.getYear(this)

val Date.month2: Int
    get() = DateUtils.getMonth(this)

val Date.day2: Int
    get() = DateUtils.dayOfMonth(this)

val Date.week: Int
    get() = DateUtils.dayOfWeek(this)

val Date.isToday: Boolean
    get() = DateUtils.isToday(this)

val Date.isAm: Boolean
    get() = DateUtils.isAm(this)

/**
 * 周
 */
val Date.simpleWeek: String
    get() = DateUtils.getSimpleWeek(this)

/**
 * 星期
 */
val Date.fullWeek: String
    get() = DateUtils.getFullWeek(this)

/**
 * 是否闰年
 */
val Date.isLeapYear: Boolean
    get() = DateUtils.isLeapYear(this)

fun Date.toString(pattern: String): String {
    return DateUtils.date2String(this, pattern)
}

fun String.toDate(pattern: String = DateUtils.YMD_HMS): Date {
    return SimpleDateFormat(pattern, Locale.getDefault()).parse(this)!!
}

fun Long.toDate(): Date {
    return Date(this)
}

fun Date.isSameDay(date: Date): Boolean {
    return DateUtils.isSameDay(this, date)
}