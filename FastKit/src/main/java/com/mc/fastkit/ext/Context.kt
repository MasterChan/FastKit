package com.mc.fastkit.ext

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import com.mc.fastkit.utils.ActivityUtils

/**
 * 屏幕宽度
 */
val Context.screenWidth: Int
    get() {
        return resources.displayMetrics.widthPixels
    }

/**
 * 屏幕高度，未包含系统装饰
 */
val Context.screenHeight: Int
    get() {
        return resources.displayMetrics.heightPixels
    }

/**
 * 屏幕物理高度，即真正的高度
 */
val Context.screenRealHeight: Int
    get() {
        val manager = getSystemService(WindowManager::class.java)
        return if (Build.VERSION.SDK_INT > Build.VERSION_CODES.R) {
            manager?.currentWindowMetrics?.bounds?.height() ?: 0
        } else {
            val metrics = DisplayMetrics()
            manager?.defaultDisplay?.getRealMetrics(metrics)
            metrics.heightPixels
        }
    }

/**
 * 状态栏高度
 */
val Context.statusBarHeight: Int
    get() {
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        return app.resources.getDimensionPixelSize(resourceId)
    }

/**
 * 导航栏高度
 */
val Context.navigationBarHeight: Int
    get() {
        val resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId != 0) app.resources.getDimensionPixelSize(resourceId) else 0
    }

/**
 * 像素密度
 */
val Context.displayDensity: Float
    get() = resources.displayMetrics.density

val Context.scaledDensity: Float
    get() = resources.displayMetrics.scaledDensity

/**
 * 是否是竖屏
 */
val Context.isPortrait: Boolean
    get() {
        return resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    }

/**
 * 是否是横屏
 */
val Context.isLandscape: Boolean
    get() {
        return resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
    }

/**
 * 获取Context对应的Activity
 * @receiver Context
 * @return Activity?
 */
val Context.activity: Activity?
    get() {
        var context = this
        while (context is ContextWrapper) {
            if (context is Activity) {
                return context
            }
            context = (this as ContextWrapper).baseContext
        }
        return null
    }

fun Context.startActivity(clazz: Class<out Activity>) {
    ActivityUtils.startActivity(this, clazz)
}

inline fun <reified T : Activity> Context.startActivity(noinline params: (Intent.() -> Unit)? = null) {
    ActivityUtils.startActivity<T>(this, params)
}

inline fun <reified T : Activity> Context.startActivity(
    enterAnimate: Int,
    exitAnimate: Int,
    noinline params: (Intent.() -> Unit)? = null
) {
    ActivityUtils.startActivity<T>(this, enterAnimate, exitAnimate, params)
}

inline fun <reified T : Activity> Context.startActivity(
    vararg sharedElements: Pair<View, String>,
    noinline params: (Intent.() -> Unit)? = null
) {
    ActivityUtils.startActivity<T>(this, *sharedElements) { params?.invoke(this) }
}