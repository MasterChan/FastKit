package com.mc.fastkit.ext

import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import com.mc.fastkit.utils.ConvertUtils

/*******************************Any***************************/
fun dp2px(dp: Float): Float = ConvertUtils.dp2px(app, dp)
fun dp2px(dp: Int): Float = ConvertUtils.dp2px(app, dp)
fun dp2pxi(dp: Float): Int = ConvertUtils.dp2pxi(app, dp)
fun dp2pxi(dp: Int): Int = ConvertUtils.dp2pxi(app, dp)

fun px2dp(px: Float): Float = ConvertUtils.px2dp(app, px)
fun px2dp(px: Int): Float = ConvertUtils.px2dp(app, px)
fun px2dpi(px: Float): Int = ConvertUtils.px2dpi(app, px)
fun px2dpi(px: Int): Int = ConvertUtils.px2dpi(app, px)

fun sp2px(sp: Float): Float = ConvertUtils.sp2px(app, sp)
fun sp2px(sp: Int): Float = ConvertUtils.sp2px(app, sp)
fun sp2pxi(sp: Float): Int = ConvertUtils.sp2pxi(app, sp)
fun sp2pxi(sp: Int): Int = ConvertUtils.sp2pxi(app, sp)

fun px2sp(px: Float) = ConvertUtils.px2sp(app, px)
fun px2sp(px: Int) = ConvertUtils.px2sp(app, px)
fun px2spi(px: Float) = ConvertUtils.px2spi(app, px)
fun px2spi(px: Int) = ConvertUtils.px2spi(app, px)

/******************************Context**************************/
fun Context.dp2px(dp: Float): Float = ConvertUtils.dp2px(this, dp)
fun Context.dp2px(dp: Int): Float = ConvertUtils.dp2px(this, dp)
fun Context.dp2pxi(dp: Float): Int = ConvertUtils.dp2pxi(this, dp)
fun Context.dp2pxi(dp: Int): Int = ConvertUtils.dp2pxi(this, dp)

fun Context.px2dp(px: Float): Float = ConvertUtils.px2dp(this, px)
fun Context.px2dp(px: Int): Float = ConvertUtils.px2dp(this, px)
fun Context.px2dpi(px: Float): Int = ConvertUtils.px2dpi(this, px)
fun Context.px2dpi(px: Int): Int = ConvertUtils.px2dpi(this, px)

fun Context.sp2px(sp: Float): Float = ConvertUtils.sp2px(this, sp)
fun Context.sp2px(sp: Int): Float = ConvertUtils.sp2px(this, sp)
fun Context.sp2pxi(sp: Float): Int = ConvertUtils.sp2pxi(this, sp)
fun Context.sp2pxi(sp: Int): Int = ConvertUtils.sp2pxi(this, sp)

fun Context.px2sp(px: Float) = ConvertUtils.px2sp(this, px)
fun Context.px2sp(px: Int) = ConvertUtils.px2sp(this, px)
fun Context.px2spi(px: Float) = ConvertUtils.px2spi(this, px)
fun Context.px2spi(px: Int) = ConvertUtils.px2spi(this, px)

/******************************Fragment**************************/
fun Fragment.dp2px(dp: Float): Float = ConvertUtils.dp2px(requireContext(), dp)
fun Fragment.dp2px(dp: Int): Float = ConvertUtils.dp2px(requireContext(), dp)
fun Fragment.dp2pxi(dp: Float): Int = ConvertUtils.dp2pxi(requireContext(), dp)
fun Fragment.dp2pxi(dp: Int): Int = ConvertUtils.dp2pxi(requireContext(), dp)

fun Fragment.px2dp(px: Float): Float = ConvertUtils.px2dp(requireContext(), px)
fun Fragment.px2dp(px: Int): Float = ConvertUtils.px2dp(requireContext(), px)
fun Fragment.px2dpi(px: Float): Int = ConvertUtils.px2dpi(requireContext(), px)
fun Fragment.px2dpi(px: Int): Int = ConvertUtils.px2dpi(requireContext(), px)

fun Fragment.sp2px(sp: Float): Float = ConvertUtils.sp2px(requireContext(), sp)
fun Fragment.sp2px(sp: Int): Float = ConvertUtils.sp2px(requireContext(), sp)
fun Fragment.sp2pxi(sp: Float): Int = ConvertUtils.sp2pxi(requireContext(), sp)
fun Fragment.sp2pxi(sp: Int): Int = ConvertUtils.sp2pxi(requireContext(), sp)

fun Fragment.px2sp(px: Float) = ConvertUtils.px2sp(requireContext(), px)
fun Fragment.px2sp(px: Int) = ConvertUtils.px2sp(requireContext(), px)
fun Fragment.px2spi(px: Float) = ConvertUtils.px2spi(requireContext(), px)
fun Fragment.px2spi(px: Int) = ConvertUtils.px2spi(requireContext(), px)

/******************************View**************************/
fun View.dp2px(dp: Float): Float = ConvertUtils.dp2px(context, dp)
fun View.dp2px(dp: Int): Float = ConvertUtils.dp2px(context, dp)
fun View.dp2pxi(dp: Float): Int = ConvertUtils.dp2pxi(context, dp)
fun View.dp2pxi(dp: Int): Int = ConvertUtils.dp2pxi(context, dp)

fun View.px2dp(px: Float): Float = ConvertUtils.px2dp(context, px)
fun View.px2dp(px: Int): Float = ConvertUtils.px2dp(context, px)
fun View.px2dpi(px: Float): Int = ConvertUtils.px2dpi(context, px)
fun View.px2dpi(px: Int): Int = ConvertUtils.px2dpi(context, px)

fun View.sp2px(sp: Float): Float = ConvertUtils.sp2px(context, sp)
fun View.sp2px(sp: Int): Float = ConvertUtils.sp2px(context, sp)
fun View.sp2pxi(sp: Float): Int = ConvertUtils.sp2pxi(context, sp)
fun View.sp2pxi(sp: Int): Int = ConvertUtils.sp2pxi(context, sp)

fun View.px2sp(px: Float) = ConvertUtils.px2sp(context, px)
fun View.px2sp(px: Int) = ConvertUtils.px2sp(context, px)
fun View.px2spi(px: Float) = ConvertUtils.px2spi(context, px)
fun View.px2spi(px: Int) = ConvertUtils.px2spi(context, px)

/******************************Number**************************/
val Int.dp
    get() = ConvertUtils.dp2px(app, this)
val Int.dpi
    get() = ConvertUtils.dp2pxi(app, this)
val Float.dp
    get() = ConvertUtils.dp2px(app, this)
val Float.dpi
    get() = ConvertUtils.dp2pxi(app, this)
val Double.dp
    get() = ConvertUtils.dp2px(app, this.toFloat())
val Double.dpi
    get() = ConvertUtils.dp2pxi(app, this.toFloat())

val Int.sp
    get() = ConvertUtils.sp2px(app, this)
val Int.spi
    get() = ConvertUtils.sp2pxi(app, this)
val Float.sp
    get() = ConvertUtils.sp2px(app, this)
val Float.spi
    get() = ConvertUtils.sp2pxi(app, this)
val Double.sp
    get() = ConvertUtils.sp2px(app, this.toFloat())
val Double.spi
    get() = ConvertUtils.sp2pxi(app, this.toFloat())