package com.mc.fastkit

import android.app.Application
import com.mc.fastkit.log.LogUtils
import com.mc.fastkit.widget.ActivityStack

/**
 * FastKit
 * @author: MasterChan
 * @date: 2023-02-18 23:36
 */
class FastKit {

    companion object {
        @JvmStatic
        lateinit var app: Application

        fun init(app: Application): FastKit {
            this.app = app
            return FastKit()
        }
    }

    init {
        ActivityStack.instance.init(app)
    }

    fun configLog(predicate: LogUtils.() -> Unit) = apply {
        predicate.invoke(LogUtils)
    }
}