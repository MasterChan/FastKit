package com.mc.fastkit.utils

import android.content.Context
import com.mc.fastkit.ext.displayDensity
import com.mc.fastkit.ext.scaledDensity

/**
 * ConvertUtils
 * @author: MasterChan
 * @date: 2024-02-27 10:35
 */
object ConvertUtils {
    fun dp2px(context: Context, dp: Float): Float = dp * context.displayDensity + 0.5f
    fun dp2px(context: Context, dp: Int): Float = dp * context.displayDensity + 0.5f
    fun px2dp(context: Context, px: Float): Float = px / context.displayDensity + 0.5f
    fun px2dp(context: Context, px: Int): Float = px / context.displayDensity + 0.5f
    fun dp2pxi(context: Context, dp: Float): Int = (dp * context.displayDensity + 0.5f).toInt()
    fun dp2pxi(context: Context, dp: Int): Int = (dp * context.displayDensity + 0.5f).toInt()
    fun px2dpi(context: Context, px: Float): Int = (px / context.displayDensity + 0.5f).toInt()
    fun px2dpi(context: Context, px: Int): Int = (px / context.displayDensity + 0.5f).toInt()

    fun sp2px(context: Context, sp: Float): Float = sp * context.scaledDensity + 0.5f
    fun sp2px(context: Context, sp: Int): Float = sp * context.scaledDensity + 0.5f
    fun px2sp(context: Context, px: Float) = px / context.scaledDensity + 0.5f
    fun px2sp(context: Context, px: Int) = px / context.scaledDensity + 0.5f
    fun sp2pxi(context: Context, sp: Float): Int = (sp * context.scaledDensity + 0.5f).toInt()
    fun sp2pxi(context: Context, sp: Int): Int = (sp * context.scaledDensity + 0.5f).toInt()
    fun px2spi(context: Context, px: Float) = (px / context.scaledDensity + 0.5f).toInt()
    fun px2spi(context: Context, px: Int) = (px / context.scaledDensity + 0.5f).toInt()
}