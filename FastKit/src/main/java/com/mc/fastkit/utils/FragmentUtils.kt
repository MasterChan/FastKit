package com.mc.fastkit.utils

import android.os.Bundle
import androidx.fragment.app.Fragment

/**
 * FragmentUtils
 * @author: MasterChan
 * @date: 2024-02-23 13:32
 */
object FragmentUtils {
    inline fun <reified T : Fragment> newInstance(args: Bundle.() -> Unit): T {
        val fragment = T::class.java.getDeclaredConstructor().newInstance()
        fragment.arguments = Bundle().apply { args.invoke(this) }
        return fragment
    }
}