package com.mc.fastkit.utils

import android.graphics.Color
import androidx.annotation.ColorInt
import androidx.annotation.FloatRange

/**
 * ColorUtils
 * @author: MasterChan
 * @date: 2024-12-19 10:12
 */
object ColorUtils {

    /**
     * 根据百分比调整颜色透明度
     * @param color Int
     * @param percent Float
     * @return Int
     */
    fun getAlphaColor(@ColorInt color: Int, @FloatRange(0.0, 1.0) percent: Float): Int {
        val alpha = (Color.alpha(color) * percent).toInt()
        return Color.argb(alpha, Color.red(color), Color.green(color), Color.blue(color))
    }
}