package com.mc.fastkit.utils

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.AbsoluteSizeSpan
import android.text.style.BackgroundColorSpan
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.ImageSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StrikethroughSpan
import android.text.style.StyleSpan
import android.text.style.SubscriptSpan
import android.text.style.SuperscriptSpan
import android.text.style.URLSpan
import android.text.style.UnderlineSpan
import android.widget.TextView

/**
 * SpanUtils
 * @author: MasterChan
 * @date: 2024-08-30 16:53
 */
class SpanUtils(private val textView: TextView, text: String) {

    private var spannable = SpannableString(text)

    fun getString(): String {
        return spannable.toString()
    }

    fun getSpannableString(): SpannableString {
        return spannable
    }

    /**
     * 添加前景色
     */
    fun foregroundColor(color: Int, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            ForegroundColorSpan(color), startIndex, endIndex,
            SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 添加前景色
     */
    fun backgroundColor(color: Int, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            BackgroundColorSpan(color), startIndex, endIndex,
            SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 相对文字大小
     * @param size:例如1.2F
     */
    fun relativeTextSize(size: Float, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            RelativeSizeSpan(size), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 绝对文字大小
     * @param size:sp
     */
    fun absoluteTextSize(size: Int, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            AbsoluteSizeSpan(size), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 中划线
     */
    fun middleLine(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            StrikethroughSpan(), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 下划线
     */
    fun underLine(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            UnderlineSpan(), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 上标
     */
    fun superscript(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            SuperscriptSpan(), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 下标
     */
    fun subscript(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            SubscriptSpan(), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 粗体
     */
    fun bold(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            StyleSpan(Typeface.BOLD), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 斜体
     */
    fun italic(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            StyleSpan(Typeface.ITALIC), startIndex, endIndex,
            SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 斜粗体
     */
    fun italicAndBold(startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            StyleSpan(Typeface.BOLD_ITALIC), startIndex, endIndex,
            SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 添加图片
     * @param drawable 需要显示指定图片大小；drawable.setBounds(0, 0, 80, 80)
     */
    fun image(drawable: Drawable, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            ImageSpan(drawable), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    /**
     * 点击事件
     */
    fun clickable(startIndex: Int, endIndex: Int, clickableSpan: ClickableSpan) = apply {
        spannable.setSpan(
            clickableSpan, startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    /**
     * 超链接
     */
    fun url(address: String, startIndex: Int, endIndex: Int) = apply {
        spannable.setSpan(
            URLSpan(address), startIndex, endIndex, SpannableString.SPAN_INCLUSIVE_EXCLUSIVE
        )
        textView.movementMethod = LinkMovementMethod.getInstance()
    }

    fun setup() {
        textView.text = getSpannableString()
    }
}