package com.mc.fastkit.utils

import com.mc.fastkit.ext.app
import java.io.File

/**
 * ResourceUtils
 * @author: MasterChan
 * @date: 2024-03-18 15:56
 */
object ResourceUtils {

    fun getIdByType(name: String, typeName: String): Int {
        return app.resources.getIdentifier(name, typeName, app.packageName)
    }

    fun getIdByName(name: String): Int {
        return getIdByType(name, "id")
    }

    fun getStringIdByName(name: String): Int {
        return getIdByType(name, "string")
    }

    fun getColorIdByName(name: String): Int {
        return getIdByType(name, "color")
    }

    fun getDimenIdByName(name: String): Int {
        return getIdByType(name, "dimen")
    }

    fun getDrawableIdByName(name: String): Int {
        return getIdByType(name, "drawable")
    }

    fun getMipmapIdByName(name: String): Int {
        return getIdByType(name, "mipmap")
    }

    fun getLayoutIdByName(name: String): Int {
        return getIdByType(name, "layout")
    }

    fun getStyleIdByName(name: String): Int {
        return getIdByType(name, "style")
    }

    fun getAnimIdByName(name: String): Int {
        return getIdByType(name, "anim")
    }

    fun getMenuIdByName(name: String): Int {
        return getIdByType(name, "menu")
    }

    fun copyFileFromAssets(assetsFilePath: String, destFilePath: String): Boolean {
        var result = true
        val assets = app.assets.list(assetsFilePath)
        if (!assets.isNullOrEmpty()) {
            assets.forEach {
                result = result and copyFileFromAssets("$assetsFilePath/$it", "$destFilePath/$it")
            }
        } else {
            File(destFilePath).writeBytes(app.assets.open(assetsFilePath).readBytes())
        }
        return result
    }
}