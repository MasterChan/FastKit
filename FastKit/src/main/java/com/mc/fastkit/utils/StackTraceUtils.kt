package com.mc.fastkit.utils

/**
 * 堆栈工具类
 * @author: MasterChan
 * @date: 2023-02-21 09:45:18
 */
object StackTraceUtils {

    /**
     * 获取当前的调用堆栈
     * @param clazz 获取堆栈的类
     * @return StackTraceElement?
     */
    fun getTargetStackTraceElement(clazz: Class<*>): StackTraceElement? {
        return getTargetStackTraceElement(clazz.name)
    }

    /**
     * 获取当前的调用堆栈
     * @param className 获取堆栈的类的完整类名
     * @return StackTraceElement?
     */
    fun getTargetStackTraceElement(className: String): StackTraceElement? {
        var traceElement: StackTraceElement? = null
        var shouldTrace = false
        val stackTrace = Thread.currentThread().stackTrace
        for (stackTraceElement in stackTrace) {
            val isTargetMethod = stackTraceElement.className == className
            if (shouldTrace && !isTargetMethod) {
                traceElement = stackTraceElement
                break
            }
            shouldTrace = isTargetMethod
        }
        return traceElement
    }
}