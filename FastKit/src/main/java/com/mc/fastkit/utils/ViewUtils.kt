package com.mc.fastkit.utils

import android.graphics.PointF
import android.view.View

/**
 * ViewUtils
 * @author: MasterChan
 * @date: 2023-02-18 23:15
 */
object ViewUtils {

    /**
     * 坐标点是否在View上
     * @param view View
     * @param point PointF
     * @return Boolean
     */
    fun isPointOnView(view: View, point: PointF): Boolean {
        return isPointOnView(view, point.x, point.y)
    }

    /**
     * 坐标点是否在View上
     * @param view View
     * @param x Float
     * @param y Float
     * @return Boolean
     */
    fun isPointOnView(view: View, x: Float, y: Float): Boolean {
        val location = intArrayOf(0, 0)
        view.getLocationInWindow(location)
        val left = location[0]
        val top = location[1]
        val bottom = top + view.height
        val right = left + view.width
        return !(x > left && x < right && y > top && y < bottom)
    }
}