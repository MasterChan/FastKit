package com.mc.fastkit.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import com.mc.fastkit.ext.activity

/**
 * ActivityUtils
 * @author: MasterChan
 * @date: 2024-06-26 13:42
 */
object ActivityUtils {

    fun startActivity(context: Context, clazz: Class<out Activity>) {
        context.startActivity(Intent(context, clazz))
    }

    inline fun <reified T : Activity> startActivity(
        context: Context,
        noinline params: (Intent.() -> Unit)? = null
    ) {
        val intent = Intent(context, T::class.java)
        params?.invoke(intent)
        context.startActivity(intent)
    }

    inline fun <reified T : Activity> startActivity(
        context: Context,
        enterAnimate: Int,
        exitAnimate: Int,
        noinline params: (Intent.() -> Unit)? = null
    ) {
        val intent = Intent(context, T::class.java)
        params?.invoke(intent)
        val bundle = ActivityOptionsCompat.makeCustomAnimation(
            context, enterAnimate, exitAnimate
        ).toBundle()
        ContextCompat.startActivity(context, intent, bundle)
    }

    inline fun <reified T : Activity> startActivity(
        context: Context,
        vararg sharedElements: Pair<View, String> = arrayOf(),
        noinline params: (Intent.() -> Unit)? = null
    ) {
        val intent = Intent(context, T::class.java)
        params?.invoke(intent)
        val activity = context.activity
        if (sharedElements.isNotEmpty() && activity != null) {
            val array = Array(sharedElements.size) {
                androidx.core.util.Pair(sharedElements[it].first, sharedElements[it].second)
            }
            context.startActivity(
                intent,
                ActivityOptionsCompat.makeSceneTransitionAnimation(activity, *array).toBundle()
            )
        } else {
            context.startActivity(intent)
        }
    }
}