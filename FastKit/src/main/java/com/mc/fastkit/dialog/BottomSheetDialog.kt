package com.mc.fastkit.dialog

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.FrameLayout
import androidx.core.view.setMargins
import com.mc.fastkit.view.sheet.OnScrollListener
import com.mc.fastkit.view.sheet.OnStateChangedListener
import com.mc.fastkit.view.sheet.SheetLayout
import com.mc.fastkit.view.sheet.SheetState

/**
 * BottomSheetDialog
 * @author: MasterChan
 * @date: 2024-3-6 15:46
 */
abstract class BottomSheetDialog<T : BottomSheetDialog<T>>(context: Context) : XDialog<T>(context) {

    open val sheetLayout = SheetLayout(context)
    private var collapsedHeight: Float? = null

    init {
        this.initSheet()
    }

    protected open fun initSheet() {
        setContentGravity(Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL)
        sheetLayout.setEnableHalfMode(false)
            .setCollapsedHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
            .addOnScrollListener {
                if (collapsedHeight == null) {
                    //第一次从onLayout中返回
                    collapsedHeight = sheetLayout.collapsedHeight.toFloat()
                    return@addOnScrollListener
                }
                collapsedHeight = if (collapsedHeight!! - it >= sheetLayout.collapsedHeight) {
                    sheetLayout.collapsedHeight.toFloat()
                } else {
                    collapsedHeight!! - it
                }
                collapsedHeight = if (collapsedHeight!! <= 0) 0f else collapsedHeight
                bgAnimator!!.evaluateBgColor(collapsedHeight!! / sheetLayout.collapsedHeight)
            }
            .addOnStateChangedListener { _, newState ->
                if (newState == SheetState.Hidden) {
                    dismiss()
                }
            }
    }

    override fun setContentView(layout: Int) {
        setContentView(LayoutInflater.from(context).inflate(layout, sheetLayout, false))
    }

    override fun setContentView(contentView: View) {
        val params = contentView.layoutParams as MarginLayoutParams
        sheetLayout.layoutParams = FrameLayout.LayoutParams(params)
        params.setMargins(0)
        sheetLayout.addView(contentView)

        super.setContentView(sheetLayout)
    }

    fun setEnableDrag(enableDrag: Boolean) = apply {
        sheetLayout.setEnableDrag(enableDrag)
    }

    fun setHandleInterceptTouchEvent(intercept: Boolean) = apply {
        sheetLayout.setHandleInterceptTouchEvent(intercept)
    }

    fun setHideAble(hideAble: Boolean) = apply {
        sheetLayout.setHideAble(hideAble)
    }

    fun setEnableHalfMode(enableHalfMode: Boolean) = apply {
        sheetLayout.setEnableHalfMode(enableHalfMode)
    }

    fun setDuration(duration: Long) = apply {
        sheetLayout.setDuration(duration)
    }

    fun setCollapsedHeight(collapsedHeight: Int) = apply {
        sheetLayout.setCollapsedHeight(collapsedHeight)
    }

    fun setCollapsedRemainderMode(remainderMode: Boolean) = apply {
        sheetLayout.setCollapsedRemainderMode(remainderMode)
    }

    fun setNextStateRatio(nextStateRatio: Float) = apply {
        sheetLayout.setNextStateRatio(nextStateRatio)
    }

    fun setFlingVelocityY(flingVelocityY: Float) = apply {
        sheetLayout.setFlingVelocityY(flingVelocityY)
    }

    fun addOnStateChangedListener(listener: OnStateChangedListener) = apply {
        sheetLayout.addOnStateChangedListener(listener)
    }

    fun removeOnStateChangedListener(listener: OnStateChangedListener) = apply {
        sheetLayout.removeOnStateChangedListener(listener)
    }

    fun addOnScrollListener(listener: OnScrollListener) = apply {
        sheetLayout.addOnScrollListener(listener)
    }

    fun removeOnScrollListener(listener: OnScrollListener) = apply {
        sheetLayout.removeOnScrollListener(listener)
    }
}