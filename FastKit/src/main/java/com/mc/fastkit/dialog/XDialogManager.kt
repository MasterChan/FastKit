package com.mc.fastkit.dialog

/**
 * XDialogManager
 * @author: MasterChan
 * @date: 2024-03-15 15:13
 */
class XDialogManager private constructor() {

    private val dialogMap = mutableListOf<XDialog<*>>()

    companion object {
        val instance by lazy { XDialogManager() }
    }

    internal fun onShow(xDialog: XDialog<*>) {
        dialogMap.add(xDialog)
    }

    internal fun onDismiss(xDialog: XDialog<*>) {
        dialogMap.remove(xDialog)
    }

    fun findDialog(tag: String): XDialog<*>? {
        return dialogMap.firstOrNull { it.tag == tag }
    }

    fun findDialogs(tag: String): List<XDialog<*>> {
        return dialogMap.filter { it.tag == tag }
    }

    fun finishDialog(tag: String) {
        findDialog(tag)?.dismiss()
    }

    fun finishDialogs(tag: String) {
        findDialogs(tag).forEach { it.dismiss() }
    }

    fun topDialog(): XDialog<*>? {
        return dialogMap.lastOrNull()
    }

    fun bottomDialog(): XDialog<*>? {
        return dialogMap.firstOrNull()
    }

    fun allDialog(): List<XDialog<*>> {
        return dialogMap
    }
}