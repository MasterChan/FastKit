package com.mc.fastkit.dialog

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.viewbinding.ViewBinding
import com.mc.fastkit.ext.cast
import com.mc.fastkit.utils.KeyboardUtils
import java.lang.reflect.ParameterizedType

/**
 * BindingDialog
 * @author: MasterChan
 * @date: 2023-12-28 10:45
 */
abstract class BindingDialog<V : ViewBinding, T : BindingDialog<V, T>>(context: Context) :
    XDialog<T>(context) {

    protected val binding by lazy { getViewBinding() }
    protected open val isUpKeyboard = true

    override fun onCreate() {
        setContentView(binding.root)
        if (isUpKeyboard) {
            dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            KeyboardUtils.registerOnKeyboardChangedListener(dialog!!.window!!) { isShow, height ->
                if (isShow) {
                    getContentView()!!.animate()
                        .setDuration(250)
                        .translationY(-height.toFloat())
                        .start()
                } else {
                    getContentView()!!.animate().setDuration(250).translationY(0f).start()
                }
            }
        }
        onCreated()
    }

    abstract fun onCreated()

    protected open fun getViewBinding(): V {
        var superClass = javaClass.genericSuperclass
        while (superClass !is ParameterizedType) {
            superClass = (superClass as Class<*>).genericSuperclass
        }
        return (superClass.actualTypeArguments[0] as Class<*>).getMethod(
            "inflate", LayoutInflater::class.java, ViewGroup::class.java, Boolean::class.java
        ).invoke(null, LayoutInflater.from(context), container, false)!!.cast()
    }
}