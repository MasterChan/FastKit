package com.mc.fastkit.dialog

import android.content.Context
import android.view.View
import androidx.annotation.LayoutRes

/**
 * 可以用匿名方式实现
 * @author: MasterChan
 * @date: 2024-3-6 14:24
 */
open class SimpleDialog(
    context: Context,
    @LayoutRes private val layoutRes: Int = 0,
    private val view: View? = null
) : XDialog<SimpleDialog>(context) {

    override fun onCreate() {
        when {
            layoutRes != 0 -> setContentView(layoutRes)
            view != null -> setContentView(view)
        }
    }
}