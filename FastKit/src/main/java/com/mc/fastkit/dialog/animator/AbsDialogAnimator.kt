package com.mc.fastkit.dialog.animator

import android.view.View

/**
 * AbsDialogAnimator
 * @author: MasterChan
 * @date: 2023-12-29 10:48
 */
abstract class AbsDialogAnimator {
    var target: View? = null
    var duration: Long = 300

    abstract fun init()
    abstract fun show(onEnd: (() -> Unit)? = null)
    abstract fun dismiss(onEnd: (() -> Unit)? = null)
}