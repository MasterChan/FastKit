package com.mc.fastkit.dialog.animator

import androidx.interpolator.view.animation.FastOutSlowInInterpolator

/**
 * contentView默认动画，由不同的[AnimatorItem]组合而成
 * @author: MasterChan
 * @date: 2023-12-29 15:11
 */
open class ContentAnimator(private val scaleFactor: Float = 0.85f) : AbsDialogAnimator() {

    val animatorItems = mutableListOf<AnimatorItem>()
    private var initTranslateX = 0f
    private var initTranslateY = 0f
    private var targetTranslateX = 0f
    private var targetTranslateY = 0f
    private var initPivotX = 0f
    private var initPivotY = 0f
    private var targetPivotX = 0f
    private var targetPivotY = 0f

    override fun init() {
        initTranslateX = target!!.translationX
        initTranslateY = target!!.translationY
        targetTranslateX = initTranslateX
        targetTranslateY = initTranslateY

        initPivotX = target!!.pivotX
        initPivotY = target!!.pivotY
        targetPivotX = initPivotX
        targetPivotY = initPivotY
    }

    override fun show(onEnd: (() -> Unit)?) {
        animatorItems.forEach {
            when (it) {
                AnimatorItem.Alpha -> target!!.alpha = 0f
                AnimatorItem.Translate.Left -> targetTranslateX = -target!!.width.toFloat()
                AnimatorItem.Translate.Top -> targetTranslateY = -target!!.measuredHeight.toFloat()
                AnimatorItem.Translate.Right -> targetTranslateX = target!!.measuredWidth.toFloat()
                AnimatorItem.Translate.Bottom -> targetTranslateY =
                    target!!.measuredHeight.toFloat()

                AnimatorItem.Scale.Center -> {
                    targetPivotX = target!!.measuredWidth / 2f
                    targetPivotY = target!!.measuredHeight / 2f
                }

                AnimatorItem.Scale.TopLeft -> {
                    targetPivotX = 0f
                    targetPivotY = 0f
                }

                AnimatorItem.Scale.TopCenter -> {
                    targetPivotX = target!!.measuredWidth / 2f
                    targetPivotY = 0f
                }

                AnimatorItem.Scale.TopRight -> {
                    targetPivotX = 0f
                    targetPivotY = target!!.measuredWidth.toFloat()
                }

                AnimatorItem.Scale.BottomLeft -> {
                    targetPivotX = 0f
                    targetPivotY = target!!.measuredHeight.toFloat()
                }

                AnimatorItem.Scale.BottomCenter -> {
                    targetPivotX = target!!.measuredWidth / 2f
                    targetPivotY = target!!.measuredHeight.toFloat()
                }

                AnimatorItem.Scale.BottomRight -> {
                    targetPivotX = target!!.measuredWidth.toFloat()
                    targetPivotY = target!!.measuredHeight.toFloat()
                }

                AnimatorItem.Scale.LeftCenter -> {
                    targetPivotX = 0f
                    targetPivotY = target!!.measuredHeight.toFloat() / 2f
                }

                AnimatorItem.Scale.RightCenter -> {
                    targetPivotX = target!!.measuredWidth.toFloat()
                    targetPivotY = target!!.measuredHeight.toFloat() / 2f
                }
            }
        }
        var animate = target!!.animate()
        animatorItems.forEach {
            animate = when (it) {
                AnimatorItem.Alpha -> {
                    animate.alpha(1f)
                }

                AnimatorItem.Translate.Left,
                AnimatorItem.Translate.Top,
                AnimatorItem.Translate.Right,
                AnimatorItem.Translate.Bottom -> {
                    target!!.translationX = targetTranslateX
                    target!!.translationY = targetTranslateY
                    animate.translationX(initTranslateX).translationY(initTranslateY)
                }

                AnimatorItem.Scale.Center,
                AnimatorItem.Scale.TopLeft,
                AnimatorItem.Scale.TopCenter,
                AnimatorItem.Scale.TopRight,
                AnimatorItem.Scale.BottomLeft,
                AnimatorItem.Scale.BottomCenter,
                AnimatorItem.Scale.BottomRight,
                AnimatorItem.Scale.LeftCenter,
                AnimatorItem.Scale.RightCenter -> {
                    target!!.pivotX = targetPivotX
                    target!!.pivotY = targetPivotY
                    target!!.scaleX = scaleFactor
                    target!!.scaleY = scaleFactor
                    animate.scaleX(1f).scaleY(1f)
                }
            }
        }
        animate.interpolator = FastOutSlowInInterpolator()
        animate.duration = duration
        animate.withEndAction { onEnd?.invoke() }
        animate.start()
    }

    override fun dismiss(onEnd: (() -> Unit)?) {
        var animate = target!!.animate()
        animatorItems.forEach {
            animate = when (it) {
                AnimatorItem.Alpha -> {
                    animate.alpha(0f)
                }

                AnimatorItem.Translate.Left,
                AnimatorItem.Translate.Top,
                AnimatorItem.Translate.Right,
                AnimatorItem.Translate.Bottom -> {
                    animate.translationX(targetTranslateX).translationY(targetTranslateY)
                }

                AnimatorItem.Scale.Center,
                AnimatorItem.Scale.TopLeft,
                AnimatorItem.Scale.TopCenter,
                AnimatorItem.Scale.TopRight,
                AnimatorItem.Scale.BottomLeft,
                AnimatorItem.Scale.BottomCenter,
                AnimatorItem.Scale.BottomRight,
                AnimatorItem.Scale.LeftCenter,
                AnimatorItem.Scale.RightCenter -> {
                    animate.scaleX(scaleFactor).scaleY(scaleFactor)
                }
            }
        }
        animate.interpolator = FastOutSlowInInterpolator()
        animate.duration = duration
        animate.withEndAction {
            target!!.scaleX = 1f
            target!!.scaleY = 1f
            target!!.pivotX = initPivotX
            target!!.pivotY = initPivotY
            target!!.translationX = initTranslateX
            target!!.translationY = initTranslateY
        }
        animate.start()
    }

    fun clearAnimatorItems() {
        animatorItems.clear()
    }

    fun addAnimatorItem(item: AnimatorItem) {
        animatorItems.add(item)
    }

    fun setAnimatorItems(items: List<AnimatorItem>) {
        animatorItems.clear()
        animatorItems.addAll(items)
    }
}