package com.mc.fastkit.dialog.animator

/**
 * ContentAnimator的子项动画，可以使用不同的组合
 * @author: MasterChan
 * @date: 2024-01-02 10:20
 */
sealed class AnimatorItem {

    data object Alpha : AnimatorItem()

    sealed class Translate : AnimatorItem() {
        data object Left : Translate()
        data object Top : Translate()
        data object Right : Translate()
        data object Bottom : Translate()
    }

    sealed class Scale : AnimatorItem() {
        data object Center : Scale()
        data object TopLeft : Scale()
        data object TopCenter : Scale()
        data object TopRight : Scale()
        data object BottomLeft : Scale()
        data object BottomCenter : Scale()
        data object BottomRight : Scale()
        data object LeftCenter : Scale()
        data object RightCenter : Scale()
    }
}