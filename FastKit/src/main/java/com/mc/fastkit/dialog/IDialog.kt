package com.mc.fastkit.dialog

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.annotation.GravityInt
import com.mc.fastkit.dialog.animator.AbsDialogAnimator
import com.mc.fastkit.dialog.animator.BgAnimator
import com.mc.fastkit.ext.cast

/**
 * IDialog
 * @author: MasterChan
 * @date: 2023-12-20 14:14
 */
interface IDialog<T : IDialog<T>> {
    /**
     * contentView的宽度，null表示使用xml中设置的宽度
     */
    var contentWidth: Int?

    /**
     * contentView的高度，null表示使用xml中设置的高度
     */
    var contentHeight: Int?

    /**
     * 点击返回键消失
     */
    var isDismissOnBackPressed: Boolean

    /**
     * 点击外部消失
     */
    var isDismissOnTouchOutside: Boolean

    /**
     * 点击外部，是否收起键盘；当[isDismissOnTouchOutside]也为true时，
     * 如果此时键盘弹出，优先隐藏键盘
     */
    var isHideKeyboardOnTouchOutside: Boolean

    /**
     * 是否允许将Dialog外的事件发送到后面的视图
     */
    var isTouchThrough: Boolean

    /**
     * ContentView的x方向偏移
     */
    var offsetX: Int

    /**
     * ContentView的y方向偏移
     */
    var offsetY: Int

    /**
     * 背景的动画
     */
    var bgAnimator: BgAnimator?

    /**
     * contentView的动画
     */
    var contentAnimator: AbsDialogAnimator?

    /**
     * 动画时长
     */
    var animatorDuration: Long

    /**
     * 是否启用动画，包括ContentView的动画和背景动画
     */
    var enableAnimator: Boolean

    /**
     * 背景的颜色
     */
    var bgColor: Int

    /**
     * 背景是否高斯模糊
     */
    var isBgBlur: Boolean

    /**
     * contentView的位置，修改后会覆盖XML中的设置
     */
    var contentGravity: Int

    /**
     * 是否隐藏NavigationBar
     */
    var isHideNavigationBar: Boolean

    /**
     * NavigationBar颜色
     */
    var navigationBarColor: Int

    /**
     * NavigationBar是否是亮色，null为默认
     */
    var isNavigationBarLight: Boolean?

    /**
     * 是否隐藏StatusBar
     */
    var isHideStatusBar: Boolean

    /**
     * StatusBar颜色
     */
    var statusBarColor: Int

    /**
     * StatusBar是否是亮色，null为默认
     */
    var isStatusBarLight: Boolean?

    /**
     * 用于标识Dialog
     */
    var tag: String

    fun setContentWidth(contentWidth: Int?): T {
        this.contentWidth = contentWidth
        return this.cast()
    }

    fun setContentHeight(contentHeight: Int?): T {
        this.contentHeight = contentHeight
        return this.cast()
    }

    fun setDismissOnBackPressed(isDismissOnBackPressed: Boolean): T {
        this.isDismissOnBackPressed = isDismissOnBackPressed
        return this.cast()
    }

    fun setDismissOnTouchOutside(isDismissOnTouchOutside: Boolean): T {
        this.isDismissOnTouchOutside = isDismissOnTouchOutside
        return this.cast()
    }

    fun setHideKeyboardOnTouchOutside(isHideKeyboardOnTouchOutside: Boolean): T {
        this.isHideKeyboardOnTouchOutside = isHideKeyboardOnTouchOutside
        return this.cast()
    }

    fun setTouchThrough(isTouchThrough: Boolean): T {
        this.isTouchThrough = isTouchThrough
        return this.cast()
    }

    fun setOffsetX(offsetX: Int): T {
        this.offsetX = offsetX
        return this.cast()
    }

    fun setOffsetY(offsetY: Int): T {
        this.offsetY = offsetY
        return this.cast()
    }

    fun setContentAnimator(animator: AbsDialogAnimator): T {
        this.contentAnimator = animator
        return this.cast()
    }

    fun setBgAnimator(animator: BgAnimator): T {
        this.bgAnimator = animator
        return this.cast()
    }

    fun setAnimatorDuration(animatorDuration: Long): T {
        this.animatorDuration = animatorDuration
        return this.cast()
    }

    fun setEnableAnimator(enableAnimator: Boolean): T {
        this.enableAnimator = enableAnimator
        return this.cast()
    }

    fun setBgColor(@ColorInt maskColor: Int): T {
        this.bgColor = maskColor
        return this.cast()
    }

    fun setBgColor(maskColor: String): T {
        setBgColor(Color.parseColor(maskColor))
        return this.cast()
    }

    fun setBgBlur(isBlur: Boolean): T {
        this.isBgBlur = isBlur
        return this.cast()
    }

    fun setContentGravity(@GravityInt contentGravity: Int): T {
        this.contentGravity = contentGravity
        return this.cast()
    }

    fun setHideNavigationBar(isHideNavigationBar: Boolean): T {
        this.isHideNavigationBar = isHideNavigationBar
        return this.cast()
    }

    fun setNavigationBarColor(@ColorInt navigationBarColor: Int): T {
        this.navigationBarColor = navigationBarColor
        return this.cast()
    }

    fun setNavigationBarColor(navigationBarColor: String): T {
        setNavigationBarColor(Color.parseColor(navigationBarColor))
        return this.cast()
    }

    fun setNavigationBarLight(isNavigationBarLight: Boolean?): T {
        this.isNavigationBarLight = isNavigationBarLight
        return this.cast()
    }

    fun setHideStatusBar(isHideStatusBar: Boolean): T {
        this.isHideStatusBar = isHideStatusBar
        return this.cast()
    }

    fun setStatusBarColor(@ColorInt statusBarColor: Int): T {
        this.statusBarColor = statusBarColor
        return this.cast()
    }

    fun setStatusBarColor(statusBarColor: String): T {
        setStatusBarColor(Color.parseColor(statusBarColor))
        return this.cast()
    }

    fun setStatusBarLight(isStatusBarLight: Boolean?): T {
        this.isStatusBarLight = isStatusBarLight
        return this.cast()
    }

    fun show(tag: String? = null): T

    fun dismiss(delay: Long)

    fun dismiss()
}