package com.mc.fastkit.constant

/**
 * TimeMills
 * @author: MasterChan
 * @date: 2023-2-19 0:32
 */
object TimeMillis {
    /**
     * 一秒钟的毫秒数
     */
    const val SEC = 1 * 1000

    /**
     * 一分钟的毫秒数；60*1000
     */
    const val MIN = 60 * SEC

    /**
     * 一个小时的毫秒数；60*60*1000=3600000
     */
    const val HOUR = 60 * MIN

    /**
     * 一天的毫秒数；24*60*60*1000=86400000
     */
    const val DAY = 24 * HOUR
}